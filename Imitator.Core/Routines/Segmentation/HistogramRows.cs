﻿using Imitator.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Imitator.Core.Routines.Segmentation
{
    public static partial class Histogram
    {
		public static bool[][,] RowSegmentation(bool[,] image, int textRowCount, 
			                                    out (int Min, int Max, double Avg) interlineSize,
			                                    int methodColumnCount = 20)
		{
			// Вычисление минимальных (грубых) границ строк.
			// Их количество в два раза больше, чем textRowCount.
			int[] boundaries = GetBoundaries(image, image.Width(), textRowCount);

			// Группировка границ в пары.
			// Их количество равно textRowCount.
			var boundaryPairs = GetBoundaryPairs(boundaries);

			// Сохраение информации об удаленённости строк друг от друга.
			List<int> interlineList = new List<int>();

			for (int i = 0; i < boundaryPairs.Length - 1; i++)
			{
				interlineList.Add(boundaryPairs[i + 1].Prev - boundaryPairs[i].Prev);
			}

			interlineSize = interlineList.Count() == 0
				? (0, 0, 0)
				: (interlineList.Min(), interlineList.Max(), interlineList.Average());

			// Для каждой колонки
			// вычисление её диапазона, относительно исходного изображения,
			// и индексов линий, разделяющих строки.
			// Количество разделительных линий на 1 меньше, чем textRowCount.
			int widthStep = image.Width() / methodColumnCount;
			(int[] Separators, Range WidthRange)[] columns = new (int[] Separators, Range WidthRange)[methodColumnCount];

			Parallel.For(0, methodColumnCount, i =>
			{
				int columnStart = i * widthStep;
				Range widthRange = columnStart..(i + 1 == methodColumnCount ? ^0 : (columnStart + widthStep));
				bool[,] window = image.GetWindowMatrix(.., widthRange);
				int[] windowHistogram = GetHistogram(window);
				int[] separators = GetTextRowSeparators(windowHistogram, boundaryPairs);
				columns[i] = (separators, widthRange);
			});

			// Получение матрицы, содержащей исходное изображение с разделителями строк.
			//bool[,] imageWithSeparators = (bool[,])image.Clone();
			//
			//foreach (var column in columns)
			//{
			//	imageWithSeparators = GetImageWithLinesByRow(imageWithSeparators, column.Separators, column.WidthRange);
			//}

			// Изменение границ строк в соответствии с индексами разделителей.
			// Если прошлые границы были минимальными, то новые должны полностью
			// содержать в себе все буквы строки.
			Parallel.For(0, boundaryPairs.Length - 1, i =>
			{
				var result = from column in columns
							 orderby column.Separators[i] ascending
							 select column.Separators[i];

				boundaryPairs[i].Ahead = result.Last();
				boundaryPairs[i + 1].Prev = result.First();
			});

			boundaryPairs[0].Prev = 0;
			boundaryPairs[^1].Ahead = image.Height();

			// Получение матрицы, содержащей исходное изображение с новыми границами.
			//List<int> tmp = new List<int>();

			//foreach (var pair in boundaryPairs)
			//{
			//	tmp.Add(pair.Prev);
			//	tmp.Add(pair.Ahead);
			//}

			//bool[,] imageWithNewBoundaries = GetImageWithLinesByRow(image, tmp.ToArray());

			// Получение построчных матриц.
			bool[][,] textRows = new bool[boundaryPairs.Length][,];

			Parallel.ForEach(boundaryPairs, (pair, state, i) =>
			{
				bool[,] textRow = new bool[pair.Ahead - pair.Prev, image.Width()];
				textRow.Init(true);

				foreach (var column in columns)
				{
					bool[,] window;

					if (i == 0)
					{
						window = image.GetWindowMatrix(..column.Separators[i], column.WidthRange);
						window.InsertTo(textRow, 0, column.WidthRange.Start.Value);
					}
					else if (i == boundaryPairs.Length - 1)
					{
						window = image.GetWindowMatrix(column.Separators[i - 1].., column.WidthRange);
						window.InsertTo(textRow, column.Separators[i - 1] - pair.Prev, column.WidthRange.Start.Value);
					}
					else
					{
						window = image.GetWindowMatrix(column.Separators[i - 1]..column.Separators[i], column.WidthRange);
						window.InsertTo(textRow, column.Separators[i - 1] - pair.Prev, column.WidthRange.Start.Value);
					}
				}

				textRows[i] = textRow;
			});

			return textRows;
		}

		public static bool[,] GetMatrixWithBoundaries(bool[,] image, int textRowCount)
		{
			//int[] histogram = GetHistogram(image);
			int[] boundaries = GetBoundaries(image, image.Width(), textRowCount);

			return GetImageWithLinesByRow(image, boundaries);
		}

		public static bool[,] GetImageFromTextRows(bool[][,] textRows, bool withSeparators = true)
		{
			if (textRows.Length == 0)
			{
				return new bool[0, 0];
			}

			int summaryHeight = withSeparators ? (textRows.Length - 1) : 0;
			summaryHeight += (from row in textRows
							  select row.Height()).Sum();

			bool[,] image = new bool[summaryHeight, textRows[0].Width()];
			int currentHeight = 0;

			for (int i = 0; i < textRows.Length; i++)
			{
				textRows[i].InsertTo(image, currentHeight, 0);
				currentHeight += textRows[i].Height();

				if (withSeparators && i != textRows.Length - 1)
				{
					for (int j = 0; j < textRows[0].Width(); j++)
					{
						image[currentHeight, j] = false;
					}

					currentHeight++;
				}
			}

			return image;
		}

		private static int[] GetHistogramSeparatorsSimple(int[] histogram, (int Min, int Max) threshold, int minimalDistance, int separatorCount)
		{
			List<int> separators = new List<int>();

			for (int i = threshold.Min; i < threshold.Max; i++)
			{
				for (int j = 0; j < histogram.Length; j++)
				{
					if (histogram[j] < i
						&& (separators.Count == 0 || Math.Abs(j - separators[^1]) > minimalDistance))
					{
						separators.Add(j);
					}
				}

				if (separatorCount == separators.Count)
				{
					break;
				}

				separators.Clear();
			}

			return separators.ToArray();
		}

		private static int[] GetTextRowSeparators(int[] histogram, (int Prev, int Ahead)[] boundaryPairs)
		{
			List<int> separators = new List<int>();

			for (int i = 0; i < boundaryPairs.Length - 1; i++)
			{
				int minIndex = boundaryPairs[i].Ahead;

				for (int j = boundaryPairs[i].Ahead + 1; j < boundaryPairs[i + 1].Prev; j++)
				{
					if (histogram[j] < histogram[minIndex])
					{
						minIndex = j;
					}
				}

				separators.Add(minIndex);
			}

			return separators.ToArray();
		}

		private static int[] GetBoundaries(bool[,] window, int thresholdMax, int textRowCount)
		{
			int[] histogram = GetHistogram(window);
			List<int> boundaries = new List<int>();

			for (int threshold = thresholdMax; threshold >= 0; threshold--)
			{
				for (int i = 1; i < histogram.Length; i++)
				{
					if (Math.Abs(histogram[i] - histogram[i - 1]) > threshold
						&& (boundaries.Count == 0 || Math.Abs(i - 1 - boundaries[^1]) > MinimalCharacterHeight))
					{
						boundaries.Add(i - 1);
					}
				}

				if (textRowCount * 2 == boundaries.Count)
				{
					return boundaries.ToArray();
				}

				boundaries.Clear();
			}

			window = TrimWindow(window, out int hMin);
			double hAvg = window.Height() / (textRowCount + 1);

			for (int i = 0; i < textRowCount; i++)
			{
				boundaries.Add((int)((i + 1) * hAvg + hMin - MinimalCharacterHeight / 2));
				boundaries.Add((int)((i + 1) * hAvg + hMin + MinimalCharacterHeight / 2));
			}

			return boundaries.ToArray();
		}

		private static (int Prev, int Ahead)[] GetBoundaryPairs(int[] boundaries)
		{
			(int Prev, int Ahead)[] pairs = new (int Prev, int Ahead)[boundaries.Length >> 1];
			int k = 0;

			for (int i = 0; i < boundaries.Length - 1; i += 2)
			{
				pairs[k++] = (boundaries[i], boundaries[i + 1]);
			}

			return pairs;
		}
	}
}
