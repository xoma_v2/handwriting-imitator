﻿using Imitator.Core.Extensions;
using Imitator.Core.Routines.Filtering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;

namespace Imitator.Core.Routines.Segmentation
{
	public static partial class Histogram
	{
		public static int MinimalCharacterHeight = 15;
		public static int MinimalCharacterWidth = 10;

		private static bool[,] GetImageWithLinesByRow(bool[,] image, int[] lineIndices, Range? range = null)
		{
			bool[,] imageWithLines = (bool[,])image.Clone();
			(int from, int to) = (range ?? ..).GetValues(imageWithLines.Width());

			foreach (int index in lineIndices)
			{
				for (int i = from; i < to; i++)
				{
					if (index < image.Height())
					{
						imageWithLines[index, i] = false;
					}
				}
			}

			return imageWithLines;
		}

		private static bool[,] GetImageWithLinesByColumn(bool[,] image, int[] lineIndices, Range? range = null)
		{
			bool[,] imageWithLines = (bool[,])image.Clone();
			(int from, int to) = (range ?? ..).GetValues(imageWithLines.Height());

			foreach (int index in lineIndices)
			{
				for (int i = from; i < to; i++)
				{
					if (index < image.Width())
					{
						imageWithLines[i, index] = false;
					}
				}
			}

			return imageWithLines;
		}

		/// <summary> Получение гистограммы интенсивностей. </summary>
		public static int[] GetHistogram(bool[,] image, bool byRow = true)
		{
			int[] histogram = new int[byRow ? image.Height() : image.Width()];

			for (int i = 0; i < image.Height(); i++)
			{
				for (int j = 0; j < image.Width(); j++)
				{
					if (image[i, j] == false)
					{
						histogram[byRow ? i : j]++;
					}
				}
			}

			return histogram;
		}

		public static bool[,] HistogramVisualization(int[] histogram)
        {
			if (histogram.Min() < 0) throw new ArgumentException(nameof(histogram));

			bool[,] image = new bool[histogram.Length, histogram.Max()];
			image.Init(true);

			for (int i = 0; i < histogram.Length; i++)
            {
				for (int j = 0; j < histogram[i]; j++)
                {
					image[i, j] = false;
                }
            }

			return image;
        }
	}
}
