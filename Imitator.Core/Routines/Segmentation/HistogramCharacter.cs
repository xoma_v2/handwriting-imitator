﻿using Imitator.Core.Extensions;
using Imitator.Core.Routines.Tree;
using LeNet5.Models.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imitator.Core.Routines.Segmentation
{
    public static partial class Histogram
    {
        public static (bool[,] Matrix, int TopOffset)[] CharacterSegmentation(bool[,] word, List<char> characters)
        {
            if (characters.Count == 0) throw new ArgumentException(nameof(characters));

            // Первый метод. Показал плохой результат.
            /*while (characters.Count != 0)
            {
                int avgCharWidth = word.Width() / characters.Count;

                if (characters.Count > 1)
                {
                    var left = FindCharacter(word, characters[0], (avgCharWidth >> 1, avgCharWidth << 1), 0, recognizer);
                    var right = FindCharacter(word, characters[^1], (word.Width() - (avgCharWidth >> 1) - 1, word.Width() - (avgCharWidth << 1) - 1), word.Width(), recognizer);

                    if (left.MinError < right.MinError)
                    {
                        characterImages.Add(left.Window);
                        word = word.GetWindowMatrix(.., left.Index..);
                        characters.RemoveAt(0);
                    }
                    else
                    {
                        characterImages.Add(right.Window);
                        word = word.GetWindowMatrix(.., ..right.Index);
                        characters.RemoveAt(characters.Count - 1);
                    }
                }
                else
                {
                    characterImages.Add(word);
                    characters.RemoveAt(0);
                }
            }*/

            // Второй метод.
            List<(bool[,] Matrix, int TopOffset)> characterImages = new List<(bool[,] Matrix, int TopOffset)>();
            List<int> separators = GetCharacterSeparators(word, characters);

            for (int i = 1; i < separators.Count; i++)
            {
                bool[,] window = word.GetWindowMatrix(.., separators[i - 1]..separators[i]);
                bool[,] windowTrimmed = TrimWindow(window, out int topOffset);

                if (windowTrimmed.Length == 0)
                {
                    windowTrimmed = window;
                    topOffset = 0;
                }

                characterImages.Add((windowTrimmed, topOffset));
            }

            return characterImages.ToArray();
        }

        private static List<int> GetCharacterSeparators(bool[,] word, List<char> characters)
        {
            if (characters.Count == 0) throw new ArgumentException(nameof(characters));

            LeNetRecognizer recognizer = new LeNetRecognizer("LeNetModels/Binary/m0.dat",
                "LeNetModels/Binary/m1.dat", "LeNetModels/Binary/m2.dat", "LeNetModels/Binary/m3.dat",
                "LeNetModels/Binary/m4.dat", "LeNetModels/Binary/m5.dat", "LeNetModels/Binary/m6.dat");
            object syncStorage = new object();
            Dictionary<(int LeftIndex, int RightIndex, char Character), double> storаge = new Dictionary<(int LeftIndex, int RightIndex, char Character), double>();
            TreeNode<double, int> root = new TreeNode<double, int>(word.Width());

            FindCharactersRecursive(word, GetHistogram(word, false), characters, characters.Count - 1, true, recognizer, root, storаge, syncStorage);

            int[] path = TreeNode.DijkstraPath(root);
            var nodes = root.Flatten().ToArray();
            List<int> separators = new List<int>();

            foreach (var index in path)
            {
                separators.Add(nodes[index].Value);
            }

            return separators;
        }

        /// <summary>
        /// Метод для рекурсивного поиска символов на изображении.
        /// Результатом является дерево, где вершины - это точки разбиения,
        /// а веса рёбер - ошибка классификатора.
        /// </summary>
        /// <param name="word"> Чёрное слово на белом фоне, представленное в виде матрицы. </param>
        /// <param name="histogram"> Горизонтальная гистограмма слова. </param>
        /// <param name="characters"> Массив искомых символов. </param>
        /// <param name="characterIndex"> Индекс текущего искомого символа. </param>
        /// <param name="rightToLeft"> Набравление движения. </param>
        /// <param name="recognizer"> Обученнный классификатор. </param>
        /// <param name="node"> Результирующее дерево. </param>
        /// <param name="storаge"> Словарь, кэширующий ошибку классификатора </param>
        /// <param name="syncStorage"> Объект для синхронизации доступа к словарю. </param>
        private static void FindCharactersRecursive(bool[,] word, int[] histogram, List<char> characters, int characterIndex,
                                                    bool rightToLeft, LeNetRecognizer recognizer, TreeNode<double, int> node,
                                                    Dictionary<(int LeftIndex, int RightIndex, char Character), double> storаge, object syncStorage)
        {
            if (histogram.Length != word.Width()) throw new ArgumentException($"{nameof(word)}, {nameof(histogram)}");

            // Подсчёт количества символов, которые ещё необходимо сегментировать.
            int remainCharacterCount = rightToLeft
                ? characterIndex + 1
                : characters.Count - characterIndex;

            if (remainCharacterCount == 0)
            {
                return;
            }

            // Список, содержащий индексы, по которым будет происходит разбиение слова.
            List<int> iterations = new List<int>();
            // Средняя длина символа.
            int avgCharWidth = word.Width() / characters.Count;

            if (remainCharacterCount == 1)
            {
                // Если символ последний, то точка для сегментации только одна -
                // граница слова.
                iterations.Add(rightToLeft ? 0 : word.Width());
            }
            else
            {
                // Пересчёт средней длины символа с учётам ширины
                // несегментированной области.
                avgCharWidth = rightToLeft
                    ? node.Value / remainCharacterCount
                    : (word.Width() - node.Value) / remainCharacterCount;

                // Расчёт половины средней длины символа.
                int avgHalfCharWidth = Math.Min(Math.Max(avgCharWidth >> 1, MinimalCharacterWidth), avgCharWidth);

                // Диапазон для поиска символа под индексом characterIndex.
                (int From, int To) border = rightToLeft
                    ? (node.Value - avgHalfCharWidth - 1, node.Value - (int)(avgCharWidth * 1.75) - 1)
                    : (node.Value + avgHalfCharWidth, node.Value + (int)(avgCharWidth * 1.75));

                // Расчёт порогового значения.
                int[] tmp = histogram[rightToLeft ? (border.To + 1)..(border.From + 1) : border.From..border.To];
                int tmpMax = tmp.Max();
                int tmpMin = tmp.Min();
                double threshold = tmpMin + (tmpMax - tmpMin) * 0.1;

                // Добавление потенциальных точек сегментации.
                foreach (var i in Range(border.From, border.To, rightToLeft ? -1 : +1))
                {
                    if (histogram[i] <= threshold)
                    {
                        iterations.Add(i);
                    }
                }

                // Удаление точки разбиения, если у неё есть предыдущий сосед, и его интенсивность ниже.
                for (int i = 1; i < iterations.Count; i++)
                {
                    int k = i;

                    while (k < iterations.Count
                           && Math.Abs(iterations[k - 1] - iterations[k]) == 1
                           && histogram[iterations[k - 1]] < histogram[iterations[k]])
                    {
                        k++;
                    }

                    iterations.RemoveRange(i, k - i);
                }
                
                Debug.Assert(iterations.Count != 0);

                // 100,000 выбрано по той причине, что при большем количестве элементов в графе,
                // поиск кратчайшего пути методом Дейкстры становится слишком долгим.
                iterations = iterations.OrderBy(i => histogram[i]).Take((int)Math.Pow(100000, 1.0 / characters.Count)).ToList();
            }

            object syncTree = new object();

            Parallel.ForEach(iterations, i =>
            {
                double error;
                bool storageSuccess = false;
                TreeNode<double, int> child;

                lock (syncStorage)
                {
                    storageSuccess = storаge.TryGetValue(
                        rightToLeft
                            ? (i, node.Value, characters[characterIndex])
                            : (node.Value, i, characters[characterIndex]),
                        out error);
                }

                if (!storageSuccess)
                {
                    // Получение окна для классификации.
                    bool[,] window = word.GetWindowMatrix(.., rightToLeft ? i..node.Value : node.Value..i);

                    // Усечение окна от белых областей и шума.
                    bool[,] windowTrimmed = TrimWindow(window, out _);

                    // Если окно было усечено полностью, то будет попытка предпринята
                    // попытка сегментировать новое окно, ширина которого равна средней
                    // ширине символа.
                    if (windowTrimmed.Length == 0)
                    {
                        if (remainCharacterCount != 1)
                        {
                            i = rightToLeft
                                ? node.Value - avgCharWidth
                                : node.Value + avgCharWidth;
                            window = word.GetWindowMatrix(.., rightToLeft ? i..node.Value : node.Value..i);
                            windowTrimmed = TrimWindow(window, out _);
                            
                            if (windowTrimmed.Length == 0)
                            {
                                windowTrimmed = window;
                            }
                        }
                        else
                        {
                            windowTrimmed = window;
                        }
                    }

                    // Изменения размера окна до размера изображения в базе данных MNIST и HRCC.
                    bool[,] windowResized = windowTrimmed.Resize(28, 28, true, true);

                    // Инвертирование цвета как в базе данных MNIST и HRCC.
                    windowResized.Invert();

                    // Помещение изображения на область размером 32x32, как того требует
                    // свёрточная нейронная сеть LeNet-5.
                    bool[,] imageToRecognize = new bool[32, 32];
                    windowResized.InsertTo(imageToRecognize, 2, 2);

                    // Распознование.
                    (_, error) = recognizer.Recognize(imageToRecognize.ToByteMatrix(), characters[characterIndex]);

                    lock (syncStorage)
                    {
                        storаge.TryAdd(
                            rightToLeft
                                ? (i, node.Value, characters[characterIndex])
                                : (node.Value, i, characters[characterIndex]),
                            error);
                    }
                }

                lock (syncTree)
                {
                    child = node.AddChild(error, i);
                }

                FindCharactersRecursive(word, histogram, characters, characterIndex + (rightToLeft ? -1 : +1), rightToLeft, recognizer, child, storаge, syncStorage);
            });
        }

        public static void CharacterSegmentationTest(string sourceDir, string outputDir)
        {
            Directory.CreateDirectory(outputDir);

            foreach (string fullname in Directory.GetFiles(sourceDir))
            {
                bool[,] word = new Bitmap(fullname).ToGreyscaleMatrix().ToBoolMatrix();
                word = TrimWindow(word, out _, false);
                string filename = Path.GetFileNameWithoutExtension(fullname);
                List<char> characters = filename.Where(c => Imitator.WorkingSet.Contains(c.ToLower())).ToList();
                MinimalCharacterWidth = (int)(word.Width() / (double)characters.Count * 0.33333);
                List<int> separators = GetCharacterSeparators(word, characters);
                word = GetImageWithLinesByColumn(word, separators.ToArray());
                word.ToBinaryBitmap().Save(Path.Combine(outputDir, $"{filename}.bmp"), ImageFormat.Bmp);
            }
        }

        /// <summary> Метод для поиска символа на изображении. </summary>
        /// <param name="word"> Чёрное слово на белом фоне, представленное в виде матрицы. </param>
        /// <param name="character"> Искомый символ. </param>
        /// <param name="border1Range">
        /// Диапазон варьирования первой границы.
        /// Значение from - включённое, значение to - исключённое. </param>
        /// <param name="border2Index">
        /// Индекс второй границы.
        /// Если первая граница варьируется слева направо, то этот индекс должен быть левее диапазона.
        /// Если первая граница варьируется справа налево, то этот индекс должен быть правее диапазона.
        /// </param>
        /// <param name="recognizer"> Обученнный классификатор. </param>
        private static (double MinError, int Index, bool[,] Window) FindCharacter(bool[,] word, char character, (int From, int To) border1Range, int border2Index, LeNetRecognizer recognizer)
        {
            int step = border1Range.To > border1Range.From ? +1 : -1;

            if (step > 0 && border2Index >= border1Range.From) throw new ArgumentException(nameof(border2Index));
            if (step < 0 && border2Index <= border1Range.From) throw new ArgumentException(nameof(border2Index));

            (double MinError, int Index, bool[,] Window) separator = (double.MaxValue, border1Range.From, new bool[0, 0]);

            foreach (int i in Range(border1Range.From, border1Range.To, step))
            {
                bool[,] window = word.GetWindowMatrix(.., step > 0 ? (border2Index..i) : (i..border2Index));
                bool[,] windowTrimmed = TrimWindow(window, out _);

                if (windowTrimmed.Length != 0)
                {
                    bool[,] windowResized = windowTrimmed.Resize(28, 28, true, true);
                    windowResized.Invert();
                    bool[,] imageToRecognize = new bool[32, 32];
                    windowResized.InsertTo(imageToRecognize, 2, 2);

                    (bool isCorrect, double error) = recognizer.Recognize(imageToRecognize.ToByteMatrix(), character);

                        Directory.CreateDirectory($"Output/Characters/Debug/{word.Width()}_{character}");
                        windowTrimmed.ToBinaryBitmap().Save($"Output/Characters/Debug/{word.Width()}_{character}/{error / 100}_{isCorrect}_trimmed.bmp", ImageFormat.Bmp);
                        imageToRecognize.ToBinaryBitmap().Save($"Output/Characters/Debug/{word.Width()}_{character}/{error / 100}_{isCorrect}_toRegognize.bmp", ImageFormat.Bmp);

                        if (error < separator.MinError)
                        {
                            separator = (error, i, windowTrimmed);
                        }

                }
            }

            return separator;
        }

        private static IEnumerable<int> Range(int fromInclusive, int toExclusive, int step = 1)
        {
            if (step == 0) throw new ArgumentException(nameof(step));

            if (step > 0)
            {
                for (int i = fromInclusive; i < toExclusive; i += step)
                {
                    yield return i;
                }
            }
            else
            {
                for (int i = fromInclusive; i > toExclusive; i += step)
                {
                    yield return i;
                }
            }
        }
    }
}
