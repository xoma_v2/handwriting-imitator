﻿using Imitator.Core.Extensions;
using Imitator.Core.Routines.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imitator.Core.Routines.Segmentation
{
	public static partial class Histogram
	{
		/// <summary>
		/// Получение массива словесных матриц.
		/// Порядок следования матриц - по строкам.
		/// </summary>
		/// <param name="textRows">
		/// Матрицы текстовых строк.
		/// Из матриц могут быть удалены шумы,
		/// ширина горизонтального профиля которых меньше MinimalCharacterWidth.
		/// </param>
		/// <param name="wordCountByRow">
		/// Массив, содержащий информацию о количество слов на каждой строке.
		/// </param>
		/// <param name="imageWithAllSeparators">
		/// Матрица, содержащая изображения с разделителями как по строкам, так и по словам.
		/// </param>
		/// <param name="spaceSize">
		/// Кортеж с информацией о размере пробелов, характерной для сегментируемого изображение.
		/// </param>
		public static bool[][,] WordSegmentation(bool[][,] textRows, int[] wordCountByRow,
			                                     out bool[,] imageWithAllSeparators,
												 out (int Min, int Max, double Avg) spaceSize)
		{
			if (wordCountByRow.Length != textRows.Length)
			{
				throw new ArgumentException($"Длина массива {nameof(textRows)} не равна длине массива {nameof(wordCountByRow)}.");
			}

			bool[][,] rowsWithSeparators = new bool[textRows.Length][,];
			bool[][,] words = new bool[wordCountByRow.Sum()][,];
			List<int> spaceList = new List<int>();
			object sync = new object();

			Parallel.For(0, textRows.Length, index =>
			{
				// Прежде чем вычислять области нулевой интенсивности необходимо гарантировать,
				// что слева и справа от сроки есть вертикальные области, состоящие только из
				// пикселей белого цвета.
				bool[,] extendedWindow = new bool[textRows[index].Height(), textRows[index].Width() + 2];
				extendedWindow.Init(true);
				textRows[index].InsertTo(extendedWindow, 0, 1);

				// Получение областей нулевой интенсивности.
				List<(int StartIndex, int Length)> areas = GetZeroAreas(extendedWindow, false, true);

				// Вычисление индексов разделителей слов.
				int[] separators = (from area in areas.GetRange(1, areas.Count - 2)
									orderby area.Length descending
									select (area.StartIndex + (area.Length >> 1))).Take(wordCountByRow[index] - 1).ToArray();

				// Сохраение информации о длинах пробелов.
				lock (sync)
				{
					spaceList.AddRange(areas.GetRange(1, areas.Count - 2).Select(area => area.Length));
				}

				// Получение строки с разделителями слов.
				rowsWithSeparators[index] = GetImageWithLinesByColumn(textRows[index], separators);

				// Получение матриц слов.
				Array.Sort(separators);
				int wordsBehindCount = wordCountByRow.Take(index).Sum();

				for (int i = 0; i < separators.Length + 1; i++)
				{
					bool[,] window;

					if (i == 0 && separators.Length == 0)
					{
						window = textRows[index];
					}
					else if (i == 0)
					{
						window = textRows[index].GetWindowMatrix(.., ..separators[i]);
					}
					else if (i == separators.Length)
					{
						window = textRows[index].GetWindowMatrix(.., separators[i - 1]..);
					}
					else
					{
						window = textRows[index].GetWindowMatrix(.., separators[i - 1]..separators[i]);
					}

					words[wordsBehindCount + i] = TrimWindow(Median.GetFilteredMatrix(window), out _);
				}
			});

			imageWithAllSeparators = GetImageFromTextRows(rowsWithSeparators);
			spaceSize = spaceList.Count() == 0
				? (0, 0, 0)
				: (spaceList.Min(), spaceList.Max(), spaceList.Average());

			return words;
		}

		private static List<(int StartIndex, int Length)> GetZeroAreas(bool[,] image, bool byRow = true, bool cleanImage = false)
		{
			// Получение гистограммы.
			int[] histogram = GetHistogram(image, byRow);

			// Получение гистограммы, фильтрованной средним фильтром.
			int[] hist = new int[histogram.Length];

			if (hist.Length > 1)
			{
				hist[0] = (histogram[0] + histogram[1]) >> 1;
				hist[^1] = (histogram[^2] + histogram[^1]) >> 1;
			}

			for (int i = 1; i < histogram.Length - 1; i++)
			{
				hist[i] = (histogram[i - 1] + histogram[i] + histogram[i + 1]) / 3;
			}

			// Вычисление областей нулевой интенсивности.
			List<(int StartIndex, int Length)> areas = new List<(int StartIndex, int Length)>();
			bool isPrevZero = false;

			for (int i = 0; i < hist.Length; i++)
			{
				if (hist[i] == 0)
				{
					if (isPrevZero)
					{
						areas[^1] = (areas[^1].StartIndex, areas[^1].Length + 1);
					}
					else
					{
						areas.Add((i, 1));
						isPrevZero = true;
					}
				}
				else
				{
					isPrevZero = false;
				}
			}

			// Расширение первой и последней областей до границ изображения,
			// если расстояние между областью и границей меньше длины стороны одного символа.
			if (areas.Count != 0)
			{
				if (areas[0].StartIndex < (byRow ? MinimalCharacterHeight : MinimalCharacterWidth))
				{
					// Очистка исходного изображения от шума.
					if (cleanImage)
					{
						bool[,] window = byRow
							? new bool[areas[0].StartIndex, image.Width()]
							: new bool[image.Height(), areas[0].StartIndex];
						window.Init(true);
						window.InsertTo(image, 0, 0);
					}

					areas[0] = (0, areas[0].Length + areas[0].StartIndex);
				}

				if (byRow)
				{
					if (image.Height() - areas[^1].StartIndex < MinimalCharacterHeight)
					{
						// Очистка исходного изображения от шума.
						if (cleanImage)
						{
							bool[,] window = new bool[image.Height() - areas[^1].StartIndex, image.Width()];
							window.Init(true);
							window.InsertTo(image, areas[^1].StartIndex, 0);
						}

						areas[^1] = (areas[^1].StartIndex, image.Height() - areas[^1].StartIndex);
					}
				}
				else
				{
					if (image.Width() - areas[^1].StartIndex < MinimalCharacterWidth)
					{
						// Очистка исходного изображения от шума.
						if (cleanImage)
						{
							bool[,] window = new bool[image.Height(), image.Width() - areas[^1].StartIndex];
							window.Init(true);
							window.InsertTo(image, 0, areas[^1].StartIndex);
						}

						areas[^1] = (areas[^1].StartIndex, image.Width() - areas[^1].StartIndex);
					}
				}
			}

			// Объединение областей интенсивности, которые находятся на расстоянии,
			// которое меньше длины стороны одного символа.
			for (int i = areas.Count - 1; i >= 1; i--)
			{
				int delta = areas[i].StartIndex - areas[i - 1].StartIndex - areas[i - 1].Length;

				if (delta < (byRow ? MinimalCharacterHeight : MinimalCharacterWidth))
				{
					// Очистка исходного изображения от шума.
					if (cleanImage)
					{
						if (byRow)
						{
							bool[,] window = new bool[delta, image.Width()];
							window.Init(true);
							window.InsertTo(image, areas[i - 1].StartIndex + areas[i - 1].Length, 0);
						}
						else
						{
							bool[,] window = new bool[image.Height(), delta];
							window.Init(true);
							window.InsertTo(image, 0, areas[i - 1].StartIndex + areas[i - 1].Length);
						}
					}

					// Объединение областей.
					areas[i - 1] = (areas[i - 1].StartIndex, areas[i - 1].Length + delta + areas[i].Length);
					areas.RemoveAt(i);
				}
			}

			return areas;
		}

		/// <summary>
		/// Усечение окна.
		/// </summary>
		public static bool[,] TrimWindow(bool[,] window, out int hMin, bool verticalNoizeReduction = true)
		{
			    hMin = 0;
			int hMax = window.Height();
			int wMin = 0;
			int wMax = window.Width();

			if (verticalNoizeReduction)
            {
				List<(int StartIndex, int Length)> areasVertical = GetZeroAreas(window);

				if (areasVertical.Sum(area => area.Length) == window.Height())
                {
					return new bool[0, 0];
                }

				if (areasVertical.Count > 0)
				{
					if (areasVertical[0].StartIndex == 0)
					{
						hMin += areasVertical[0].Length;
					}

					if ((areasVertical[^1].StartIndex + areasVertical[^1].Length) == hMax)
					{
						hMax -= areasVertical[^1].Length;
					}
				}
			}
			else
            {
				int[] histVertical = GetHistogram(window);

				if (histVertical.Sum() == 0)
                {
					return new bool[0, 0];
                }

				for (int i = 0; i < histVertical.Length && histVertical[i] == 0; i++)
				{
					hMin++;
				}

				for (int i = histVertical.Length - 1; i > -1 && histVertical[i] == 0; i--)
				{
					hMax--;
				}
			}

			int[] histHorizontal = GetHistogram(window, false);

			if (histHorizontal.Sum() == 0)
			{
				return new bool[0, 0];
			}

			for (int i = 0; i < histHorizontal.Length && histHorizontal[i] == 0; i++)
			{
				wMin++;
			}

			for (int i = histHorizontal.Length - 1; i >= 0 && histHorizontal[i] == 0; i--)
			{
				wMax--;
			}

			return window.GetWindowMatrix(hMin..hMax, wMin..wMax);
		}
	}
}
