﻿using Imitator.Core.Extensions;
using System;
using System.Threading.Tasks;

namespace Imitator.Core.Routines.Binarization
{
    public static class Niblack
    {
        /// <summary>
        /// Локальная чёрно-белая бинаризация изображения методом Ниблэка.
        /// </summary>
        /// <param name="greyscaleMatrix">
        /// Изображение в оттенках серого в виде матрицы.
        /// </param>
        public static bool[,] GetBinaryMatrix(byte[,] greyscaleMatrix, double k = -0.2, int area = 15)
        {
            int height = greyscaleMatrix.Height();
            int width = greyscaleMatrix.Width();
            bool[,] binaryMatrix = new bool[height, width];

            Parallel.For(0, height, h =>
            {
                for (int w = 0; w < width; w++)
                {
                    binaryMatrix[h, w] = GetBinaryPixel(h, w, greyscaleMatrix, k, area);
                }
            });

            return binaryMatrix;
        }

        /// <summary>
        /// Определение цвета пикселя.
        /// </summary>
        private static bool GetBinaryPixel(int i, int j, byte[,] greyscaleMatrix, double k = -0.2, int area = 15)
        {
            byte[] intensities = greyscaleMatrix.GetWindowArrayByRow(i, j, area);
            double average = Average(intensities);
            double rms = RMS(intensities, average);
            byte threshold = (byte)(average + k * rms);

            return greyscaleMatrix[i, j] > threshold;
        }

        /// <summary>
        /// Среднее значение.
        /// </summary>
        private static double Average(byte[] intensities)
        {
            int sum = 0;

            foreach (byte intensity in intensities)
            {
                sum += intensity;
            }

            return sum / (double)intensities.Length;
        }

        /// <summary>
        /// Среднеквадратическое отклонение.
        /// </summary>
        private static double RMS(byte[] intensities, double average)
        {
            double tmp = 0;

            foreach (byte intensity in intensities)
            {
                tmp += Math.Pow(intensity, 2);
            }

            return Math.Sqrt(tmp / intensities.Length - Math.Pow(average, 2));
        }
    }
}
