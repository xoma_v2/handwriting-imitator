﻿using Imitator.Core.Extensions;

namespace Imitator.Core.Routines.Binarization
{
    public static class Otsu
    {
        /// <summary>
        /// Глобальная чёрно-белая бинаризация изображения методом Оцу.
        /// </summary>
        /// <param name="greyscaleMatrix">
        /// Изображение в оттенках серого в виде матрицы.
        /// </param>
        public static bool[,] GetBinaryMatrix(byte[,] greyscaleMatrix)
        {
            byte[] greyscaleArray = greyscaleMatrix.ToArrayByRow(sizeof(byte));
            byte threshold = GetThreshold(greyscaleArray);
            bool[,] binaryMatrix = Binarization(greyscaleMatrix, threshold);

            return binaryMatrix;
        }

        /// <summary> Получение гистограммы интенсивностей. </summary>
        private static int[] GetHistogram(byte[] image)
        {
            int[] histogram = new int[256];

            for (int i = 0; i < image.Length; i++)
            {
                histogram[image[i]]++;
            }

            return histogram;
        }

        /// <summary> Расчёт суммы всех интенсивностей. </summary>
        private static int GetIntensitySum(byte[] image)
        {
            int sum = 0;

            for (int i = 0; i < image.Length; i++)
            {
                sum += image[i];
            }    

            return sum;
        }

        /// <summary> Получение порога бинаризации методом Оцу. </summary>
        /// <param name="image"> Изображение в оттенках серого в виде массива по строкам. </param>
        private static byte GetThreshold(byte[] image)
        {
            int[] histogram = GetHistogram(image);

            // Необходимо для быстрого пересчета разности дисперсий между классами.
            int itensitySum = GetIntensitySum(image);

            byte bestThresh = 0;
            double bestSigma = 0;

            int firstClassPixelCount = 0;
            int firstClassIntensitySum = 0;

            // При 255 в ноль уходит знаменатель внутри for.
            for (byte thresh = 0; thresh < 255; thresh++)
            {
                firstClassPixelCount += histogram[thresh];
                firstClassIntensitySum += thresh * histogram[thresh];

                double firstClassProb = firstClassPixelCount / (double)image.Length;
                double secondClassProb = 1 - firstClassProb;

                double firstClassMean = firstClassIntensitySum / (double)firstClassPixelCount;
                double secondClassMean = (itensitySum - firstClassIntensitySum) / (double)(image.Length - firstClassPixelCount);
                
                double meanDelta = firstClassMean - secondClassMean;

                double sigma = firstClassProb * secondClassProb * meanDelta * meanDelta;

                if (sigma > bestSigma)
                {
                    bestSigma = sigma;
                    bestThresh = thresh;
                }
            }

            return bestThresh;
        }

        /// <summary> Чёрно-белая бинаризация. </summary>
        /// <param name="greyscaleMatrix"> Изображение в оттенках серого в виде матрицы. </param>
        /// <param name="threshold"> Пороговое значение. </param>
        private static bool[,] Binarization(byte[,] greyscaleMatrix, byte threshold)
        {
            bool[,] binaryMatrix = new bool[greyscaleMatrix.Height(), greyscaleMatrix.Width()];

            for (int i = 0; i < greyscaleMatrix.Height(); i++)
            {
                for (int j = 0; j < greyscaleMatrix.Width(); j++)
                {
                    binaryMatrix[i, j] = greyscaleMatrix[i, j] > threshold;
                }
            }

            return binaryMatrix;
        }
    }
}
