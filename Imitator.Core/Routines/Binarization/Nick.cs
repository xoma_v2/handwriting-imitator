﻿using Imitator.Core.Extensions;
using System;
using System.Threading.Tasks;

namespace Imitator.Core.Routines.Binarization
{
    public static class Nick
    {
        /// <summary>
        /// Локальная чёрно-белая бинаризация изображения методом NICK.
        /// </summary>
        /// <param name="greyscaleMatrix">
        /// Изображение в оттенках серого в виде матрицы.
        /// </param>
        public static bool[,] GetBinaryMatrix(byte[,] greyscaleMatrix, double k = -0.1, int area = 19)
        {
            int height = greyscaleMatrix.Height();
            int width = greyscaleMatrix.Width();
            bool[,] binaryMatrix = new bool[height, width];

            Parallel.For(0, height, h =>
            {
                for (int w = 0; w < width; w++)
                {
                    binaryMatrix[h, w] = GetBinaryPixel(h, w, greyscaleMatrix, k, area);
                }
            });

            return binaryMatrix;
        }

        /// <summary>
        /// Определение цвета пикселя.
        /// </summary>
        private static bool GetBinaryPixel(int i, int j, byte[,] greyscaleMatrix, double k = -0.1, int area = 19)
        {
            byte[] intensities = greyscaleMatrix.GetWindowArrayByRow(i, j, area);
            double average = Average(intensities);
            double nickFactor = NickFactor(intensities, average);
            byte threshold = (byte)(average + k * nickFactor);

            return greyscaleMatrix[i, j] > threshold;
        }

        /// <summary>
        /// Среднее значение.
        /// </summary>
        private static double Average(byte[] intensities)
        {
            int sum = 0;

            foreach (byte intensity in intensities)
            {
                sum += intensity;
            }

            return sum / (double)intensities.Length;
        }

        /// <summary>
        /// Коэффициент метода NICK.
        /// </summary>
        private static double NickFactor(byte[] intensities, double average)
        {
            double tmp = 0;

            foreach (byte intensity in intensities)
            {
                tmp += Math.Pow(intensity, 2);
            }

            return Math.Sqrt((tmp - Math.Pow(average, 2)) / intensities.Length);
        }
    }
}
