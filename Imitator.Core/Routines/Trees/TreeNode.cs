﻿using LeNet5.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Imitator.Core.Routines.Tree
{
    public class TreeNode<TWeight, TValue>
    {
        public TWeight Weight { get; }
        public TValue Value { get; }
        public TreeNode<TWeight, TValue>? Parent { get; }

        private readonly List<TreeNode<TWeight, TValue>> childrens;

        public TreeNode<TWeight, TValue> this[int i] => childrens[i];
        public ReadOnlyCollection<TreeNode<TWeight, TValue>> Childrens => childrens.AsReadOnly();

        public TreeNode(TWeight weight, TValue value, TreeNode<TWeight, TValue> parent)
        {
            Weight = weight;
            Value = value;
            Parent = parent;
            childrens = new List<TreeNode<TWeight, TValue>>();
        }

        public TreeNode(TValue value) : this(default, value)
        { }

        public TreeNode(TWeight weight, TValue value)
        {
            Weight = weight;
            Value = value;
            Parent = null;
            childrens = new List<TreeNode<TWeight, TValue>>();
        }

        public TreeNode<TWeight, TValue> AddChild(TWeight weight, TValue value)
        {
            var node = new TreeNode<TWeight, TValue>(weight, value, this);
            childrens.Add(node);
            return node;
        }

        public TreeNode<TWeight, TValue>[] AddChildrens(params (TWeight Weight, TValue Value)[] items)
        {
            return items.Select(item => AddChild(item.Weight, item.Value)).ToArray();
        }

        public bool RemoveChild(TreeNode<TWeight, TValue> node) => childrens.Remove(node);

        public void Traverse(Action<TWeight, TValue> action)
        {
            action(Weight, Value);

            foreach (var child in childrens)
            {
                child.Traverse(action);
            }
        }

        public IEnumerable<TreeNode<TWeight, TValue>> Flatten()
        {
            return new[] { this }.Concat(childrens.SelectMany(x => x.Flatten()));
        }
    }

    public static class TreeNode
    {
        public static double[] DijkstraDistances<KValue>(TreeNode<double, KValue> tree)
        {
            var nodes = tree.Flatten().ToList();

            if (nodes.Where(node => node.Weight < 0).Count() > 0) throw new ArgumentException(nameof(tree));

            if (nodes.Count == 0)
            {
                return new double[0];
            }

            double[] distances = new double[nodes.Count];
            distances.Init(double.PositiveInfinity);
            distances[0] = 0;
            bool[] used = new bool[nodes.Count];

            for (int i = 0; i < nodes.Count; i++)
            {
                int k = -1;

                for (int j = 0; j < nodes.Count; j++)
                {
                    if (!used[j] && (k == -1 || distances[j] < distances[k]))
                    {
                        k = j;
                    }
                }

                if (double.IsInfinity(distances[k]))
                {
                    break;
                }

                used[k] = true;
                int childrenCount = nodes[k].Childrens.Count;

                for (int j = 0; j < childrenCount; j++)
                {
                    int edgeTo = nodes.FindIndex(node => node == nodes[k][j]);

                    if (distances[k] + nodes[k][j].Weight < distances[edgeTo])
                    {
                        distances[edgeTo] = distances[k] + nodes[k][j].Weight;
                    }
                }
            }

            return distances;
        }

        /// <summary>
        /// Поиск кратчайшего пути до первого узла, не имеющего потомков.
        /// </summary>
        public static int[] DijkstraPath<KValue>(TreeNode<double, KValue> tree)
        {
            var nodes = tree.Flatten().ToList();

            if (nodes.Where(node => node.Weight < 0).Count() > 0) throw new ArgumentException(nameof(tree));

            if (nodes.Count == 0)
            {
                return new int[0];
            }

            double[] distances = DijkstraDistances(tree);
            List<(TreeNode<double, KValue> Node, double Distance, int Index)> selectedList = new List<(TreeNode<double, KValue> Node, double Distance, int Index)>();

            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].Childrens.Count == 0)
                {
                    selectedList.Add((nodes[i], distances[i], i));
                }
            }

            var selected = selectedList.OrderBy(item => item.Distance).First();
            int endIndex = selected.Index;
            double endDistance = selected.Distance;
            List<int> path = new List<int>(); // Список посещенных вершин.
            path.Add(endIndex); // Начальный элемент - индекс конечной вершины.

            while (endIndex != 0) // Пока не дошли до начальной вершины...
            {
                for (int i = 0; i < nodes.Count; i++) // Просматриваем все вершины...
                {
                    int childrenCount = nodes[i].Childrens.Count;

                    for (int j = 0; j < childrenCount; j++)
                    {
                        if (nodes[i][j] == nodes[endIndex]) // Если есть связь...
                        {
                            double tmp = endDistance - nodes[endIndex].Weight; // Определяем вес пути до предыдущей вершины...

                            if (Math.Abs(tmp - distances[i]) < 0.00001) // Если вес совпал с рассчитанным,
                            {                                           // значит из этой вершины и был переход.
                                endDistance = tmp; // Сохраняем новый вес,
                                endIndex = i;      // сохраняем предыдущую вершину
                                path.Add(i);       // и добавляем её в список посещённых вершин.
                            }
                        }
                    }
                }
            }

            return path.ToArray();
        }
    }
}
