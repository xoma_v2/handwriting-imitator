﻿using Imitator.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imitator.Core.Routines.Filtering
{
    public static class Median
    {
        public static int[,] GetFilteredMatrix(int[,] argbMatrix, int area = 1)
        {
            int height = argbMatrix.Height();
            int width = argbMatrix.Width();
            int[,] filteredMatrix = new int[height, width];

            Parallel.For(0, height, h =>
            {
                for (int w = 0; w < width; w++)
                {
                    filteredMatrix[h, w] = GetPixelMedian(h, w, argbMatrix, area);
                }
            });

            return filteredMatrix;
        }

        public static byte[,] GetFilteredMatrix(byte[,] greyscaleMatrix, int area = 1)
        {
            int height = greyscaleMatrix.Height();
            int width = greyscaleMatrix.Width();
            byte[,] filteredMatrix = new byte[height, width];

            Parallel.For(0, height, h =>
            {
                for (int w = 0; w < width; w++)
                {
                    filteredMatrix[h, w] = GetPixelMedian(h, w, greyscaleMatrix, area);
                }
            });

            return filteredMatrix;
        }

        public static bool[,] GetFilteredMatrix(bool[,] binaryMatrix, int area = 1)
        {
            int height = binaryMatrix.Height();
            int width = binaryMatrix.Width();
            bool[,] filteredMatrix = new bool[height, width];

            Parallel.For(0, height, h =>
            {
                for (int w = 0; w < width; w++)
                {
                    filteredMatrix[h, w] = GetPixelMedian(h, w, binaryMatrix, area);
                }
            });

            return filteredMatrix;
        }

        private static byte AverageComponent(byte a, byte b) => (byte)Math.Sqrt((a * a + b * b) >> 1);

        private static int GetAverageColor(int a, int b)
        {
            Color x = Color.FromArgb(a);
            Color y = Color.FromArgb(b);

            byte alpha = AverageComponent(x.A, y.A);
            byte red = AverageComponent(x.R, y.R);
            byte green = AverageComponent(x.G, y.G);
            byte blue = AverageComponent(x.B, y.B);

            return Color.FromArgb(alpha, red, green, blue).ToArgb();
        }

        private static int GetPixelMedian(int i, int j, int[,] matrix, int area = 1)
        {
            int[] neighbours = matrix.GetWindowArrayByRow(i, j, area);
            Array.Sort(neighbours);
            int length = neighbours.Length;

            if (length % 2 == 0)
            {
                int a = neighbours[(length >> 1) - 1];
                int b = neighbours[length >> 1];

                return GetAverageColor(a, b);
            }

            return neighbours[length >> 1];
        }

        private static byte GetPixelMedian(int i, int j, byte[,] matrix, int area = 1)
        {
            byte[] neighbours = matrix.GetWindowArrayByRow(i, j, area);
            Array.Sort(neighbours);
            int length = neighbours.Length;

            if (length % 2 == 0)
            {
                byte a = neighbours[(length >> 1) - 1];
                byte b = neighbours[length >> 1];

                return AverageComponent(a, b);
            }

            return neighbours[length >> 1];
        }

        private static bool GetPixelMedian(int i, int j, bool[,] matrix, int area = 1)
        {
            bool[] neighbours = matrix.GetWindowArrayByRow(i, j, area);
            Array.Sort(neighbours);
            int length = neighbours.Length;

            return neighbours[length >> 1];
        }
    }
}
