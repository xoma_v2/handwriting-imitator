﻿using Imitator.Core.Extensions;
using System;
using System.Drawing.Imaging;
using System.IO;

namespace Imitator.Core
{
    class Program
    {
        static void Main(string[] args)
        {
            string inPath = "Assets/img3.jpg";
            string outputDir = "Output";

            string[][] text2 = new string[][]
            {
                new string[] { "Сейчас", "сложилась", "несколько" },
                new string[] { "парадоксальная", "ситуация", "в" },
                new string[] { "университетах", "образовались", "наряду" },
                new string[] { "с", "традиционными", "математиче" },
                new string[] { "скими", "факультетами", "факуль" },
                new string[] { "теты", "прикладной", "матема" },
                new string[] { "тики", "хотя", "нет", "факультетов" },
                new string[] { "прикладной", "физики", "биологии" },
                new string[] { "или", "экономики", "В", "какой" },
                new string[] { "то", "степени", "это", "объясняется" },
                new string[] { "тем", "что", "роль", "последних", "выпол" },
                new string[] { "няют", "технические", "сельскохозяй" },
                new string[] { "ственный", "медицинские", "эконо" },
                new string[] { "мические", "институты", "Они" },
                new string[] { "вышли", "из", "системы", "университетского" },
                new string[] { "образования", "десятки", "а", "то", "и" },
                new string[] { "сотни", "лет", "тому", "назад" }
            };

            string[][] text3 = new string[][]
            {
                new string[] { "Широкая", "электрификация", "южных", "губерний" },
                new string[] { "даст", "мощный", "толчок", "подъёму", "сельского" },
                new string[] { "хозяйства", "Борец", "за", "идею", "Чучхэ" },
                new string[] { "выступил", "с", "гиком", "шумом", "жаром", "и" },
                new string[] { "фырканьем", "на", "съезде", "и", "в", "ящик" }
            };

            if (!Imitator.TrySegmentate(inPath, outputDir, text3, log => Console.WriteLine(log)))
            {
                return;
            }

            Console.WriteLine("Банк символов создан. Проверьте корректность сегментации и удалите некорректные символы.\n");
            Console.WriteLine("Нажмите любую клавишу для продолжения...");
            Console.ReadKey(true);

            ImitatorData meta;
            try
            {
                meta = Imitator.GetData(Path.Combine(outputDir, Path.GetFileNameWithoutExtension(inPath), "characters"));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"\nВ процессе загрузки данных для имитации произошла ошибка.\n{ex.Message}");
                return;
            }

            try
            {
                //bool[,] imitatedtext = Imitator.Imitate(meta, "Имитируем текст\nнанананананананананана\nнескольких строках");
                string textToImitate = @"Широкая электрификация южных губерний
даст мощный толчок подъёму сельского
хозяйства Борец за идею Чучхэ
выступил с гиком шумом жаром и
фырканьем на съезде и в ящик";
                byte[,] imitatedtext = Imitator.Imitate(meta, textToImitate.Replace("\r", ""));
                imitatedtext.ToGreyscaleBitmap().Save($"{outputDir}/result.bmp", ImageFormat.Bmp);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"\nВ процессе имитации почерка произошла ошибка.\n{ex.Message}");
                return;
            }

            Console.Write("Нажмите любую клавишу для завершения...");
            Console.ReadKey(true);
        }
    }
}
