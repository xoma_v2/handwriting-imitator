﻿using Imitator.Core.Extensions;
using Imitator.Core.Routines.Segmentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Imitator.Core
{
    public class ImitatorData
    {
        public readonly (int Min, int Max, double Avg) InterlineSize;
        public readonly (int Min, int Max, double Avg) SpaceSize;
        public readonly List<Character> Characters;
        public readonly List<Character> LowerCharacters;
        public readonly List<Character> UpperCharacters;
        public readonly Character AvgTemplate;
        public readonly Character AvgLower;
        public readonly Character AvgUpper;

        public ImitatorData((int Min, int Max, double Avg) interlineSize,
                            (int Min, int Max, double Avg) spaceSize,
                             List<Character> characters)
        {
            if (interlineSize.Min > interlineSize.Max
                || interlineSize.Avg > interlineSize.Max
                || interlineSize.Min > interlineSize.Avg) throw new ArgumentException(nameof(interlineSize));

            if (spaceSize.Min > spaceSize.Max
                || spaceSize.Avg > spaceSize.Max
                || spaceSize.Min > spaceSize.Avg) throw new ArgumentException(nameof(spaceSize));

            InterlineSize = interlineSize;
            SpaceSize = spaceSize;
            Characters = characters;
            LowerCharacters = characters.Where(character => character.Label.IsLower()).ToList();
            UpperCharacters = characters.Where(character => character.Label.IsUpper()).ToList();

            bool[,] templateMatrix = new bool[Histogram.MinimalCharacterHeight, Histogram.MinimalCharacterWidth];
            AvgTemplate = new Character('_', templateMatrix, 0, null, null);

            if (LowerCharacters.Count() == 0)
            {
                AvgLower = AvgTemplate;
            }
            else
            {
                double avgHeight = LowerCharacters.Average(character => character.Matrix.Height());
                double avgWidth = LowerCharacters.Average(character => character.Matrix.Width());
                double avgOffset = LowerCharacters.Average(characters => characters.TopOffset);
                bool[,] lowerMatrix = new bool[(int)Math.Round(avgHeight), (int)Math.Round(avgWidth)];
                AvgLower = new Character('_', lowerMatrix, (int)Math.Round(avgOffset), null, null);
            }

            if (UpperCharacters.Count() == 0)
            {
                AvgUpper = AvgTemplate;
            }
            else
            {
                double avgHeight = UpperCharacters.Average(character => character.Matrix.Height());
                double avgWidth = UpperCharacters.Average(character => character.Matrix.Width());
                double avgOffset = UpperCharacters.Average(characters => characters.TopOffset);
                bool[,] upperMatrix = new bool[(int)Math.Round(avgHeight), (int)Math.Round(avgWidth)];
                AvgUpper = new Character('_', upperMatrix, (int)Math.Round(avgOffset), null, null);
            }
        }

        public List<Character> FindSimilar(char label, char? leftNeighbour, char? rightNeighbour)
        {
            var coincidence0 = Characters.Where(character => character.Label == label
                                                && character.LeftNeighbour == leftNeighbour
                                                && character.RightNeighbour == rightNeighbour);

            if (coincidence0.Count() != 0)
            {
                return coincidence0.ToList();
            }

            var coincidence1 = Characters.Where(character => character.Label == label
                                                && (character.LeftNeighbour == leftNeighbour
                                                    || character.RightNeighbour == rightNeighbour));

            if (coincidence1.Count() != 0)
            {
                return coincidence1.ToList();
            }

            var coincidence2 = Characters.Where(character => character.Label == label);

            if (coincidence2.Count() != 0)
            {
                return coincidence2.ToList();
            }

            label = label.IsUpper()
                ? label.ToLower()
                : label.ToUpper();

            var coincidence3 = Characters.Where(character => character.Label == label
                                                && character.LeftNeighbour == leftNeighbour
                                                && character.RightNeighbour == rightNeighbour);

            if (coincidence3.Count() != 0)
            {
                return coincidence2.ToList();
            }

            var coincidence4 = Characters.Where(character => character.Label == label
                                                && (character.LeftNeighbour == leftNeighbour
                                                    || character.RightNeighbour == rightNeighbour));

            if (coincidence4.Count() != 0)
            {
                return coincidence4.ToList();
            }

            var coincidence5 = Characters.Where(character => character.Label == label);

            if (coincidence5.Count() != 0)
            {
                return coincidence5.ToList();
            }

            List<Character> characters = new List<Character>()
            {
                label.IsUpper() ? AvgLower : AvgUpper
            };

            return characters;
        }
    }
}
