﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Imitator.Core.Extensions
{
    public static class BitmapExtension
    {
        /// <summary>
        /// Кортеж интенсивностей по компонентам RGB.
        /// Сумма интенсивностей не должна превышать 1.
        /// </summary>
        public static (double R, double G, double B) GreyIntensities = (0.299, 0.587, 0.114);

        /// <summary> Получения цветного изображения формата ARGB в виде матрицы. </summary>
        /// <param name="image"> Исходное изображение. </param>
        public static int[,] ToArgbMatrix(this Bitmap image)
        {
            int[,] argbMatrix = new int[image.Height, image.Width];

            for (int h = 0; h < image.Height; h++)
            {
                for (int w = 0; w < image.Width; w++)
                {
                    argbMatrix[h, w] = image.GetPixel(w, h).ToArgb();
                }
            }

            return argbMatrix;
        }

        public static Bitmap ChangePixelFormat(this Bitmap image, PixelFormat format)
        {
            Bitmap newImage = new Bitmap(image.Width, image.Height, format);

            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.DrawImage(image, new Rectangle(0, 0, newImage.Width, newImage.Height));
            }

            return newImage;
        }

        /// <summary> Получение изображения в оттенках серого в виде массива по строкам. </summary>
        /// <param name="image"> Исходное изображение. </param>
        public static byte[] ToGreyscaleArray(this Bitmap image)
        {
            return ToGreyscaleArray(image, GreyIntensities);
        }

        /// <summary> Получение изображения в оттенках серого в виде массива по строкам. </summary>
        /// <param name="image"> Исходное изображение. </param>
        /// <param name="intensities">
        /// Кортеж интенсивностей по компонентам RGB.
        /// Сумма интенсивностей не должна превышать 1.
        /// </param>
        public static byte[] ToGreyscaleArray(this Bitmap image, (double R, double G, double B) intensities)
        {
            if (intensities.R < 0 || intensities.G < 0 || intensities.B < 0 || (intensities.R + intensities.G + intensities.B) > 1)
            {
                throw new ArgumentException($"Значения компонент параметра {nameof(intensities)} должы быть неотрицательными, а их сумма не должна превышать 1.");
            }

            BitmapData bitmapData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
            int nBytes = bitmapData.Height * bitmapData.Stride;
            byte[] imageBytes = new byte[nBytes];
            Marshal.Copy(bitmapData.Scan0, imageBytes, 0, nBytes);
            int bytesPerPixel = Image.GetPixelFormatSize(bitmapData.PixelFormat) / 8;
            byte[] greyscaleArray = new byte[bitmapData.Width * bitmapData.Height];

            Parallel.For(0, bitmapData.Height, h =>
            {
                int k = h * bitmapData.Width;
                int position = h * bitmapData.Stride;

                for (int w = 0; w < bitmapData.Width; w++)
                {
                    byte blue = imageBytes[position];
                    byte green = imageBytes[position + 1];
                    byte red = imageBytes[position + 2];

                    byte grey = (byte)(intensities.R * red + intensities.G * green + intensities.B * blue);
                    greyscaleArray[k] = grey;

                    k++;
                    position += bytesPerPixel;
                }
            });

            image.UnlockBits(bitmapData);

            return greyscaleArray;
        }

        /// <summary> Получение изображения в оттенках серого в виде матрицы. </summary>
        /// <param name="image"> Исходное изображение. </param>
        public static byte[,] ToGreyscaleMatrix(this Bitmap image)
        {
            return ToGreyscaleMatrix(image, GreyIntensities);
        }

        /// <summary> Получение изображения в оттенках серого в виде матрицы. </summary>
        /// <param name="image"> Исходное изображение. </param>
        /// <param name="intensities">
        /// Кортеж интенсивностей по компонентам RGB.
        /// Сумма интенсивностей не должна превышать 1.
        /// </param>
        public static byte[,] ToGreyscaleMatrix(this Bitmap image, (double R, double G, double B) intensities)
        {
            if (intensities.R < 0 || intensities.G < 0 || intensities.B < 0 || (intensities.R + intensities.G + intensities.B) > 1)
            {
                throw new ArgumentException($"Значения компонент параметра {nameof(intensities)} должы быть неотрицательными, а их сумма не должна превышать 1.");
            }

            BitmapData bitmapData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
            int nBytes = bitmapData.Height * bitmapData.Stride;
            byte[] imageBytes = new byte[nBytes];
            Marshal.Copy(bitmapData.Scan0, imageBytes, 0, nBytes);
            int bytesPerPixel = Image.GetPixelFormatSize(bitmapData.PixelFormat) / 8;
            byte[,] greyscaleMatrix = new byte[bitmapData.Height, bitmapData.Width];

            Parallel.For(0, bitmapData.Height, h =>
            {
                int position = h * bitmapData.Stride;

                for (int w = 0; w < bitmapData.Width; w++)
                {
                    byte blue = imageBytes[position];
                    byte green = imageBytes[position + 1];
                    byte red = imageBytes[position + 2];

                    byte grey = (byte)(intensities.R * red + intensities.G * green + intensities.B * blue);
                    greyscaleMatrix[h, w] = grey;

                    position += bytesPerPixel;
                }
            });

            image.UnlockBits(bitmapData);

            return greyscaleMatrix;
        }
    }
}
