﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imitator.Core.Extensions
{
    public static class CharExtension
    {
        public static bool IsUpper(this char character)
        {
            return char.IsUpper(character);
        }

        public static bool IsLower(this char character)
        {
            return char.IsLower(character);
        }

        public static char ToLower(this char character)
        {
            return char.ToLower(character);
        }

        public static char ToUpper(this char character)
        {
            return char.ToUpper(character);
        }
    }
}
