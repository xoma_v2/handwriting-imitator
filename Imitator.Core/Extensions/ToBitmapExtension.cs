﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;

namespace Imitator.Core.Extensions
{
    public static class ToBitmapExtension
    {
        /// <summary> Получение изображения в оттенках серого. </summary>
        /// <param name="bytes">
        /// Массив оттенков серого,
        /// в который изображение помещено по строкам.
        /// </param>
        public static Bitmap ToGreyscaleBitmap(this byte[] bytes, int width, int height)
        {
            if (bytes.Length < 1) throw new ArgumentException($"Массив {nameof(bytes)} не содержит элементов.");
            if (width < 1) throw new ArgumentOutOfRangeException($"Значение {nameof(width)} дожно быть больше 0.");
            if (height < 1) throw new ArgumentOutOfRangeException($"Значение {nameof(height)} дожно быть больше 0.");
            if (bytes.Length != width * height) throw new ArgumentException($"Размер массива {nameof(bytes)} должен быть равен произведению {nameof(width)} и {nameof(height)}.");

            byte[] tmp = new byte[bytes.Length * 3];
            for (int i = 0; i < bytes.Length; i++)
            {
                int j = i * 3;
                tmp[j] = bytes[i];
                tmp[j + 1] = bytes[i];
                tmp[j + 2] = bytes[i];
            }

            (byte[] newBytes, int newStride) = PadLines(tmp, height, width * 3);
            var binaryImage = new Bitmap(width, height);
            var data = binaryImage.LockBits(new Rectangle(Point.Empty, binaryImage.Size), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            Marshal.Copy(newBytes, 0, data.Scan0, newBytes.Length);
            binaryImage.UnlockBits(data);

            return binaryImage;
        }

        /// <summary> Получение изображения в оттенках серого. </summary>
        /// <param name="matrix">
        /// Матрица оттенков серого, в которую помещено изображение.
        /// </param>
        public static Bitmap ToGreyscaleBitmap(this byte[,] matrix)
        {
            if (matrix.Length < 1) throw new ArgumentException($"Матрица {nameof(matrix)} не содержит элементов.");

            return ToGreyscaleBitmap(matrix.ToArrayByRow(sizeof(byte)), matrix.Width(), matrix.Height());
        }

        /// <summary> Получение чёрно-белого изображения. </summary>
        /// <param name="matrix">
        /// Матрица, каждый true-элемент которой
        /// соответствует белому пикселю изображения.
        /// </param>
        public static Bitmap ToBinaryBitmap(this bool[,] matrix)
        {
            if (matrix.Length < 1) throw new ArgumentException($"Матрица {nameof(matrix)} не содержит элементов.");

            return ToGreyscaleBitmap(matrix.ToByteArray(), matrix.Width(), matrix.Height());
        }

        /// <summary> Получение цветного изображения формата ARGB. </summary>
        /// <param name="matrix"> Исходное изображение. </param>
        public static Bitmap ToArgbBitmap(this int[,] matrix)
        {
            if (matrix.Length < 1) throw new ArgumentException($"Матрица {nameof(matrix)} не содержит элементов.");

            (byte[] newBytes, int newStride) = PadLines(matrix.ToByteArrayByRow(sizeof(int)), matrix.Height(), matrix.Width() * sizeof(int));
            IntPtr newBytesPtr = Marshal.UnsafeAddrOfPinnedArrayElement(newBytes, 0);
            Bitmap binaryImage = new Bitmap(matrix.Width(), matrix.Height(), newStride, PixelFormat.Format32bppArgb, newBytesPtr);

            return binaryImage;
        }

        /// <summary>
        /// Дополняет байт-массива элементами так, чтобы его кратность
        /// стала равна 4 (ширина строки Bitmap'а должна быть кратна 4).
        /// </summary>
        private static (byte[] Bytes, int Stride) PadLines(byte[] bytes, int height, int widthInBytes)
        {
            int currentStride = widthInBytes;
            int newStride = GetStride(widthInBytes);
            byte[] newBytes = new byte[newStride * height];

            for (int i = 0; i < height; i++)
            {
                Buffer.BlockCopy(bytes, currentStride * i, newBytes, newStride * i, currentStride);
            }

            return (newBytes, newStride);
        }

        private static int GetStride(int width)
        {
            return width % 4 == 0
                ? width
                : width + (4 - (width % 4));
        }
    }
}
