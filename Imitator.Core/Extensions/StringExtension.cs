﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imitator.Core.Extensions
{
    public static class StringExtension
    {
        public static string ReplaceAll(this string str, char oldChar, char newChar)
        {
            string result = (string)str.Clone();

            while (result.Contains(oldChar))
            {
                result = result.Replace(oldChar, newChar);
            }

            return result;
        }

        public static string ReplaceAll(this string str, string oldValue, string newValue)
        {
            string result = (string)str.Clone();

            while (result.Contains(oldValue))
            {
                result = result.Replace(oldValue, newValue);
            }

            return result;
        }
    }
}
