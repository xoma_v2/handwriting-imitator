﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imitator.Core.Extensions
{
    public static class RandomExtension
    {
        /// <summary>
        /// Возвращает случайное вещественное число, которое больше или равно 0.0, и меньше чем 1.0.
        /// Среднее возвращаемое значение будет равно avgValue.
        /// </summary>
        /// <param name="avgValue"> Число в диапазоне [0; 1). </param>
        public static double NextDouble(this Random rand, double avgValue)
        {
            if (avgValue >= 1 || avgValue < 0) throw new ArgumentOutOfRangeException(nameof(avgValue));

            // https://stackoverflow.com/questions/17795265/make-random-numbers-tend-average-to-a-specific-value
            double c = 1 / avgValue - 1;

            return Math.Pow(rand.NextDouble(), c);
        }

        /// <summary>
        /// Возвращает случайное целое число, которое больше или равно minValue, и меньше чем maxValue.
        /// Среднее возвращаемое значение будет равно avgValue.
        /// </summary>
        /// <param name="avgValue"> Число в диапазоне [minValue; maxValue). </param>
        public static int Next(this Random rand, int minValue, int maxValue, double avgValue)
        {
            if (avgValue >= maxValue || avgValue < minValue) throw new ArgumentOutOfRangeException(nameof(avgValue));

            if (minValue == maxValue)
            {
                return rand.Next(minValue, maxValue);
            }

            int delta = maxValue - minValue;
            double randValue = rand.NextDouble((avgValue - minValue) / delta);

            return (int)Math.Round(randValue * delta + minValue);
        }
    }
}
