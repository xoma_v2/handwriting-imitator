﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace Imitator.Core.Extensions
{
    public static class ByteMatrixExtension
    {
        public static bool[,] ToBoolMatrix(this byte[,] matrix)
        {
            if (matrix.ToArrayByRow(sizeof(byte)).Where(value => value != 255 && value != 0).Count() != 0)
            {
                throw new ArgumentException($"Матрица {nameof(matrix)} содержит значения, отличные от 0 и от 255.");
            }

            bool[,] boolMatrix = new bool[matrix.Height(), matrix.Width()];

            for (int i = 0; i < matrix.Height(); i++)
            {
                for (int j = 0; j < matrix.Width(); j++)
                {
                    boolMatrix[i, j] = matrix[i, j] == 255;
                }
            }

            return boolMatrix;
        }

        /// <summary>
        /// Изменение размера матрицы с интерполяцией.
        /// При сохранении пропорций матрица будет выравнена по центру новой области.
        /// </summary>
        public static byte[,] Resize(this byte[,] matrix, int newDimension0, int newDimension1, bool keepProportion, byte backgroungIntensity = 255)
        {
            if (matrix.Length == 0)
            {
                byte[,] newMatrix = new byte[newDimension0, newDimension1];
                newMatrix.Init(backgroungIntensity);

                return newMatrix;
            }

            int newHeight = newDimension0;
            int newWidth = newDimension1;

            if (keepProportion)
            {
                double ratioH = newDimension0 / (double)matrix.Height();
                double ratioW = newDimension1 / (double)matrix.Width();
                double ratio = Math.Min(ratioH, ratioW);

                newHeight = (int)(matrix.Height() * ratio);
                newWidth = (int)(matrix.Width() * ratio);
            }

            if (newWidth == 0 || newHeight == 0)
            {
                byte[,] newMatrix = new byte[newDimension0, newDimension1];
                newMatrix.Init(backgroungIntensity);

                return newMatrix;
            }

            Bitmap image = new Bitmap(newWidth, newHeight);

            using (var g = Graphics.FromImage(image))
            {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(matrix.ToGreyscaleBitmap(), new Rectangle(0, 0, newWidth, newHeight));
            }

            if (keepProportion)
            {
                byte[,] newMatrix = new byte[newDimension0, newDimension1];
                newMatrix.Init(backgroungIntensity);
                image.ToGreyscaleMatrix().InsertTo(newMatrix, (newDimension0 - newHeight) >> 1, (newDimension1 - newWidth) >> 1);

                return newMatrix;
            }

            return image.ToGreyscaleMatrix();
        }
    }
}
