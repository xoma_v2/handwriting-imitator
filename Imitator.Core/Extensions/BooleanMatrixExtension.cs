﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;

namespace Imitator.Core.Extensions
{
    public static class BooleanMatrixExtension
    {
        public static void Invert(this bool[,] matrix)
        {
            for (int i = 0; i < matrix.Height(); i++)
            {
                for (int j = 0; j < matrix.Width(); j++)
                {
                    matrix[i, j] = !matrix[i, j];
                }
            }
        }

        public static byte[] ToByteArray(this bool[,] matrix)
        {
            byte[] byteArray = new byte[matrix.Length];
            int k = 0;

            foreach (bool value in matrix)
            {
                byteArray[k++] = value ? (byte)255 : (byte)0;
            }

            return byteArray;
        }

        public static byte[,] ToByteMatrix(this bool[,] matrix)
        {
            byte[,] byteMatrix = new byte[matrix.Height(), matrix.Width()];

            for (int i = 0; i < matrix.Height(); i++)
            {
                for (int j = 0; j < matrix.Width(); j++)
                {
                    byteMatrix[i, j] = matrix[i, j] ? (byte)255 : (byte)0;
                }
            }

            return byteMatrix;
        }

        /// <summary>
        /// Изменение размера матрицы с интерполяцией.
        /// При сохранении пропорций матрица будет выравнена по центру новой области.
        /// </summary>
        public static bool[,] Resize(this bool[,] matrix, int newDimension0, int newDimension1, bool keepProportion, bool isBackgroundWhite)
        {
            if (matrix.Length == 0)
            {
                bool[,] newMatrix = new bool[newDimension0, newDimension1];
                newMatrix.Init(isBackgroundWhite);

                return newMatrix;
            }

            int newHeight = newDimension0;
            int newWidth = newDimension1;

            if (keepProportion)
            {
                double ratioH = newDimension0 / (double)matrix.Height();
                double ratioW = newDimension1 / (double)matrix.Width();
                double ratio = Math.Min(ratioH, ratioW);

                newHeight = (int)(matrix.Height() * ratio);
                newWidth = (int)(matrix.Width() * ratio);
            }

            if (newWidth == 0 || newHeight == 0)
            {
                bool[,] newMatrix = new bool[newDimension0, newDimension1];
                newMatrix.Init(isBackgroundWhite);

                return newMatrix;
            }

            Bitmap image = new Bitmap(newWidth, newHeight);

            using (var g = Graphics.FromImage(image))
            {
                g.SmoothingMode = SmoothingMode.None;
                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(matrix.ToBinaryBitmap(), new Rectangle(0, 0, newWidth, newHeight));
            }

            if (keepProportion)
            {
                bool[,] newMatrix = new bool[newDimension0, newDimension1];
                newMatrix.Init(isBackgroundWhite);
                image.ToGreyscaleMatrix().ToBoolMatrix().InsertTo(newMatrix, (newDimension0 - newHeight) >> 1, (newDimension1 - newWidth) >> 1);

                return newMatrix;
            }

            return image.ToGreyscaleMatrix().ToBoolMatrix();
        }

        public static void AlphaInsertTo(this bool[,] source, bool[,] destination, int i, int j, bool alphaIs = true)
        {
            if (i + source.Height() > destination.Height()) throw new ArgumentException(nameof(i));
            if (j + source.Width() > destination.Width()) throw new ArgumentException(nameof(j));

            for (int x = i; x < i + source.Height(); x++)
            {
                for (int y = j; y < j + source.Width(); y++)
                {
                    if (alphaIs != source[x - i, y - j])
                    {
                        destination[x, y] = source[x - i, y - j];
                    }
                }
            }
        }
    }
}
