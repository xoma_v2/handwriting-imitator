﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imitator.Core.Extensions
{
    public static class MatrixExtension
    {
        public static int Height<T>(this T[,] matrix) => matrix.GetLength(0);

        public static int Width<T>(this T[,] matrix) => matrix.GetLength(1);

        public static T[] ToArrayByColumn<T>(this T[,] matrix)
        {
            T[] array = new T[matrix.Length];
            int k = 0;

            for (int j = 0; j < matrix.Width(); j++)
            {
                for (int i = 0; i < matrix.Height(); i++)
                {
                    array[k++] = matrix[i, j];
                }
            }

            return array;
        }

        public static T[] ToArrayByRow<T>(this T[,] matrix, int sizeofT)
        {
            T[] array = new T[matrix.Length];
            Buffer.BlockCopy(matrix, 0, array, 0, matrix.Length * sizeofT);

            return array;
        }

        public static byte[] ToByteArrayByRow<T>(this T[,] matrix, int sizeofT)
        {
            byte[] array = new byte[matrix.Length * sizeofT];
            Buffer.BlockCopy(matrix, 0, array, 0, matrix.Length * sizeofT);

            return array;
        }

        /// <param name="area"> Полуразмер окна (половина длины стороны квадрата). </param>
        public static T[] GetWindowArrayByRow<T>(this T[,] matrix, int i, int j, int area)
        {
            if (area == 0) throw new ArgumentOutOfRangeException($"Значение полуразмера окна {nameof(area)} не должно быть равно 0.");

            area = Math.Abs(area);

            int hMin = Math.Max(0, i - area);
            int hMax = Math.Min(matrix.Height(), i + area);

            int wMin = Math.Max(0, j - area);
            int wMax = Math.Min(matrix.Width(), j + area);

            T[] array = new T[(hMax - hMin) * (wMax - wMin)];
            int k = 0;

            for (int h = hMin; h < hMax; h++)
            {
                for (int w = wMin; w < wMax; w++)
                {
                    array[k++] = matrix[h, w];
                }
            }

            return array;
        }

        public static (int From, int To) GetValues(this Range range, int maxValue)
        {
            return (range.Start.IsFromEnd ? (maxValue - range.Start.Value) : range.Start.Value,
                    range.End.IsFromEnd ? (maxValue - range.End.Value) : range.End.Value);
        }

        public static T[,] GetWindowMatrix<T>(this T[,] matrix, Range? dimension0 = null, Range? dimension1 = null)
        {
            (int hFrom, int hTo) = (dimension0 ?? ..).GetValues(matrix.Height());
            (int wFrom, int wTo) = (dimension1 ?? ..).GetValues(matrix.Width());

            if (hFrom < 0 || hTo > matrix.Height() || (hTo - hFrom) < 0) throw new ArgumentException(nameof(dimension0));
            if (wFrom < 0 || wTo > matrix.Width() || (wTo - wFrom) < 0) throw new ArgumentException(nameof(dimension1));

            T[,] window = new T[hTo - hFrom, wTo - wFrom];

            for (int i = hFrom, h = 0; i < hTo; i++, h++)
            {
                for (int j = wFrom, w = 0; j < wTo; j++, w++)
                {
                    window[h, w] = matrix[i, j];
                }
            }

            return window;
        }

        public static void InsertTo<T>(this T[,] source, T[,] destination, int i, int j)
        {
            if (i + source.Height() > destination.Height()) throw new ArgumentException(nameof(i));
            if (j + source.Width() > destination.Width()) throw new ArgumentException(nameof(j));

            for (int x = i; x < i + source.Height(); x++)
            {
                for (int y = j; y < j + source.Width(); y++)
                {
                    destination[x, y] = source[x - i, y - j];
                }
            }
        }

        public static void Init<T>(this T[,] matrix, T value)
        {
            for (int i = 0; i < matrix.Height(); i++)
            {
                for (int j = 0; j < matrix.Width(); j++)
                {
                    matrix[i, j] = value;
                }
            }
        }
    }

}
