﻿using Imitator.Core.Extensions;
using Imitator.Core.Routines.Binarization;
using Imitator.Core.Routines.Filtering;
using Imitator.Core.Routines.Segmentation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Imitator.Core
{
    public static partial class Imitator
    {
        public static bool TrySegmentate(string inPath, string outputDir, string[][] text, Action<string> logger)
        {
            string resultDir;
            try
            {
                logger("Создание выходной директории...");
                resultDir = Path.Combine(outputDir, Path.GetFileNameWithoutExtension(inPath));
                Directory.CreateDirectory(resultDir);
                logger($"Создана директория {resultDir}\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ ходе создания выходной директории произошла ошибка.\n{ex.Message}");
                return false;
            }

            List<List<char>> words = new List<List<char>>();

            foreach (string[] row in text)
            {
                foreach (string word in row)
                {
                    List<char> characters = new List<char>();

                    foreach (char character in word)
                    {
                        characters.Add(character);
                    }

                    words.Add(characters);
                }
            }

            int[,] argbMatrix;
            Bitmap source;
            try
            {
                logger("Считывание изображения в память...");
                source = new Bitmap(inPath);
                argbMatrix = source.ToArgbMatrix();
                source.Save(Path.Combine(resultDir, "0-source.bmp"), ImageFormat.Bmp);
                logger($"Изображение сохранено в папку {resultDir} под именем 0-source.bmp\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ ходе считывания изображения произошла ошибка.\n{ex.Message}");
                return false;
            }

            byte[,] greyscaleMatrix;
            try
            {
                logger("Перевод изображения в оттенки серого...");
                greyscaleMatrix = source.ToGreyscaleMatrix();
                Bitmap grey = greyscaleMatrix.ToGreyscaleBitmap();
                grey.Save(Path.Combine(resultDir, "1-grey.bmp"), ImageFormat.Bmp);
                logger($"Изображение сохранено в папку {resultDir} под именем 1-grey.bmp\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе перевода изображения в оттенки серого произошла ошибка.\n{ex.Message}");
                return false;
            }

            try
            {
                logger("Применение медианного фильтра к изображению...");
                byte[,] filteredMatrix = Median.GetFilteredMatrix(greyscaleMatrix, Math.Max(greyscaleMatrix.Width() / 1000, 1));
                Bitmap median = filteredMatrix.ToGreyscaleBitmap();
                median.Save(Path.Combine(resultDir, "2-median.bmp"), ImageFormat.Bmp);
                logger($"Изображение сохранено в папку {resultDir} под именем 2-median.bmp\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ ходе применения медианного фильтра произошла ошибка.\n{ex.Message}");
                return false;
            }

            bool[,] binaryMatrix;
            try
            {
                logger("Бинаризация изображения методом NICK...");
                binaryMatrix = Nick.GetBinaryMatrix(greyscaleMatrix);
                Bitmap nick = binaryMatrix.ToBinaryBitmap();
                nick.Save(Path.Combine(resultDir, "3-nick.bmp"), ImageFormat.Bmp);
                logger($"Изображение сохранено в папку {resultDir} под именем 3-nick.bmp\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе бинаризации произошла ошибка.\n{ex.Message}");
                return false;
            }

            try
            {
                logger("Изменение значений констанст в соответствии с размером изображения и средним количеством символов на строке.");
                double avgCharCountPerRow = 0;
                foreach (string[] row in text)
                {
                    foreach (string word in row)
                    {
                        avgCharCountPerRow += word.Length;
                    }
                }
                avgCharCountPerRow /= text.Length;
                bool[,] roughTrim = Histogram.TrimWindow(binaryMatrix, out _, false);
                Histogram.MinimalCharacterWidth = (int)(roughTrim.Width() / avgCharCountPerRow * 0.3);
                logger($"Минимальная ширина символа = {roughTrim.Width()} / {avgCharCountPerRow} * {0.3} = {Histogram.MinimalCharacterWidth} пикселей.");
                Histogram.MinimalCharacterHeight = (int)(roughTrim.Height() / text.Length * 0.15);
                logger($"Минимальная высота символа = {roughTrim.Height()} / {text.Length} * {0.15} = {Histogram.MinimalCharacterHeight} пикселей.\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе корректировки констант произошла ошибка.\n{ex.Message}");
                return false;
            }

            //bool[,] binaryMatrixOtsu = Otsu.GetBinaryMatrix(greyscaleMatrix);
            //Bitmap otsu = binaryMatrixOtsu.ToBinaryBitmap();
            //otsu.Save($"{outputDir}/{filename}.otsu.bmp", ImageFormat.Bmp);

            //bool[,] binaryMatrixNiblack = Niblack.GetBinaryMatrix(greyscaleMatrix);
            //Bitmap niblack = binaryMatrixNiblack.ToBinaryBitmap();
            //niblack.Save($"{outputDir}/{filename}.niblack.bmp", ImageFormat.Bmp);

            try
            {
                logger("Поиск базовых линий...");
                bool[,] segmantationBoundaries = Histogram.GetMatrixWithBoundaries(binaryMatrix, text.Length);
                Bitmap rowBoundaries = segmantationBoundaries.ToBinaryBitmap();
                rowBoundaries.Save(Path.Combine(resultDir, "4-boundaries.bmp"), ImageFormat.Bmp);
                logger($"Изображение с базовыми линиями сохранено в папку {resultDir} под именем 4-boundaries.bmp\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе поиска базовых линий произошла ошибка.\n{ex.Message}");
                return false;
            }

            //bool[][,] rowMatricesSimple = Histogram.RowSegmentation(binaryMatrix, text.Length, 1);
            //Bitmap rowsSimple = Histogram.GetImageFromTextRows(rowMatricesSimple).ToBinaryBitmap();
            //rowsSimple.Save($"{outputDir}/{filename}.4-rows-1-simple.bmp", ImageFormat.Bmp);

            bool[][,] rowMatricesMulti;
            (int Min, int Max, double Avg) interlineSize;
            try
            {
                logger("Сегментация строк...");
                rowMatricesMulti = Histogram.RowSegmentation(binaryMatrix, text.Length, out interlineSize);
                Bitmap rowsMulti = Histogram.GetImageFromTextRows(rowMatricesMulti).ToBinaryBitmap();
                rowsMulti.Save(Path.Combine(resultDir, "5-rows.bmp"), ImageFormat.Bmp);
                logger($"Изображение с разделителями сохранено в папку {resultDir} под именем 5-rows.bmp\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе сегментации строк произошла ошибка.\n{ex.Message}");
                return false;
            }

            List<bool[,]> wordMatrices;
            (int Min, int Max, double Avg) spaceSize;
            try
            {
                logger("Сегментация слов...");
                bool[,] matrixWithAllSeparators;
                wordMatrices = Histogram.WordSegmentation(rowMatricesMulti, text.Select(row => row.Length).ToArray(), out matrixWithAllSeparators, out spaceSize).ToList();
                Bitmap imageWithAllSeparators = matrixWithAllSeparators.ToBinaryBitmap();
                imageWithAllSeparators.Save(Path.Combine(resultDir, "6-words.bmp"), ImageFormat.Bmp);
                logger($"Изображение с разделителями сохранено в папку {resultDir} под именем 6-words.bmp");

                Directory.CreateDirectory(Path.Combine(resultDir, "words"));

                for (int i = 0; i < wordMatrices.Count; i++)
                {
                    if (wordMatrices[i].Length == 0)
                    {
                        logger($"Не удалось распознать слово №{i + 1}, из-за чего оно было удалено из последовательности на сегментацию.");
                        words.RemoveAt(i);
                        wordMatrices.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        Bitmap word = wordMatrices[i].ToBinaryBitmap();
                        word.Save(Path.Combine(resultDir, "words", $"word-{i + 1}.bmp"), ImageFormat.Bmp);
                    }
                }

                logger($"Изображения слов сохранены в папку {Path.Combine(resultDir, "words")}\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе сегментации слов произошла ошибка.\n{ex.Message}");
                return false;
            }

            List<(bool[,] Matrix, int TopOffset)[]> charactersList;
            try
            {
                logger("Сегментация символов...");
                charactersList = new List<(bool[,] Matrix, int TopOffset)[]>();

                for (int i = 0; i < words.Count; i++)
                {
                    logger($"в слове №{i + 1}...");
                    var characters = Histogram.CharacterSegmentation(wordMatrices[i], words[i]);
                    charactersList.Add(characters);
                }
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе сегментации символов произошла ошибка.\n{ex.Message}");
                return false;
            }

            try
            {
                logger("\nСохранение изображений символов и метаинформации...");
                Directory.CreateDirectory(Path.Combine(resultDir, "characters"));
                SaveWordsWithMetadata(words, charactersList, Path.Combine(resultDir, "characters"));
                string meta = $"{interlineSize.Min}/{interlineSize.Max}/{interlineSize.Avg}\n";
                meta += $"{spaceSize.Min}/{spaceSize.Max}/{spaceSize.Avg}\n";
                File.WriteAllText(Path.Combine(resultDir, "characters", "meta.dat"), meta);
                logger($"Метаинформация и изображения сохранены в папку {Path.Combine(resultDir, "characters")}\n");
            }
            catch (Exception ex)
            {
                logger($"\nВ процессе сохранения изображений символов произошла ошибка.\n{ex.Message}");
                return false;
            }

            return true;
        }

        private static void SaveWordsWithMetadata(List<List<char>> charactersList1,
                                                  List<(bool[,] Matrix, int TopOffset)[]> charactersList2,
                                                  string outputDir)
        {
            List<List<(char Label, bool[,] Matrix, int TopOffset)>> words = new List<List<(char Label, bool[,] Matrix, int TopOffset)>>();

            for (int i = 0; i < charactersList1.Count; i++)
            {
                List<(char Label, bool[,] Matrix, int TopOffset)> word = new List<(char Label, bool[,] Matrix, int TopOffset)>();

                for (int j = 0; j < charactersList1[i].Count; j++)
                {
                    word.Add((charactersList1[i][j], charactersList2[i][j].Matrix, charactersList2[i][j].TopOffset));
                }

                words.Add(word);
            }

            var singleCharWords = from word in words
                                  where word.Count == 1
                                  select word;

            var multiCharWords = from word in words
                                 where word.Count > 1
                                 select word;

            // Для символов, которые находятся в слове...
            foreach (var word in multiCharWords)
            {
                // Расчёт среднего смещения относительно верхней границы слова.
                double avgTopOffset = word.Average(character => character.TopOffset);

                // Расчёт минимального смещения для букв, чьё смещение больше среднего.
                // Маленькое смещение - признак "высокых" букв: заглавных, 'в' с "хвостом", 'б' с "хвостом" и т.д.
                int delta = word
                    .Where(character => character.TopOffset >= avgTopOffset)
                    .Select(character => character.TopOffset)
                    .Min();

                // Пересчё смещения.
                for (int i = 0; i < word.Count; i++)
                {
                    word[i] = (word[i].Label, word[i].Matrix, word[i].TopOffset - delta);
                }
            }

            // Для отдельно стоящих символов...
            foreach (var word in singleCharWords)
            {
                // Поиск точно таких же символов.
                var similarCharacters = multiCharWords
                    .SelectMany(w => w)
                    .Where(character => character.Label == word[0].Label);

                // Если таких же символов нет,
                // то ищем либо все заглавные буквы, либо все строчные.
                if (similarCharacters.Count() == 0 && char.ToUpper(word[0].Label) == word[0].Label)
                {
                    similarCharacters = multiCharWords
                        .SelectMany(w => w)
                        .Where(character => char.ToUpper(character.Label) == character.Label);
                }
                else if (similarCharacters.Count() == 0 && char.ToUpper(word[0].Label) != word[0].Label)
                {
                    similarCharacters = multiCharWords
                        .SelectMany(w => w)
                        .Where(character => char.ToUpper(character.Label) != character.Label);
                }

                double avgTopOffset;

                if (similarCharacters.Count() != 0)
                {
                    // Похожие символы найдены.
                    avgTopOffset = similarCharacters.Average(character => character.TopOffset);
                }
                else
                {
                    // Похожих символов нет, выбираем любые символы.
                    similarCharacters = multiCharWords.SelectMany(w => w);
                    avgTopOffset = similarCharacters.Count() == 0
                        ? 0
                        : similarCharacters.Average(character => character.TopOffset);
                }

                word[0] = (word[0].Label, word[0].Matrix, (int)Math.Round(avgTopOffset));
            }

            // Сохранение изображений символов. Имя файла с изображением кодируется в формате:
            // {символ}.{левый_сосед}{правый_сосед}.{смещение}.{guid}.bmp
            // Если нет соседа, то ставится _
            foreach (var character in singleCharWords.SelectMany(w => w))
            {
                character.Matrix
                    .ToBinaryBitmap()
                    .Save($"{outputDir}/{character.Label}.__.{character.TopOffset}.{Guid.NewGuid()}.bmp", ImageFormat.Bmp);
            }

            foreach (var word in multiCharWords)
            {
                for (int i = 0; i < word.Count; i++)
                {
                    string neighbors;

                    if (i == 0)
                    {
                        neighbors = $"_{word[i + 1].Label}";
                    }
                    else if (i == word.Count - 1)
                    {
                        neighbors = $"{word[i - 1].Label}_";
                    }
                    else
                    {
                        neighbors = $"{word[i - 1].Label}{word[i + 1].Label}";
                    }

                    word[i].Matrix
                        .ToBinaryBitmap()
                        .Save($"{outputDir}/{word[i].Label}.{neighbors}.{word[i].TopOffset}.{Guid.NewGuid()}.bmp", ImageFormat.Bmp);
                }
            }
        }
    }
}
