﻿using Imitator.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Imitator.Core
{
    public static partial class Imitator
    {
        private static Random rand = new Random();

        public static char[] WorkingSet = new char[]
        {
            'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д',
            'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и',
                 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н',
            'О', 'о', 'П', 'п', 'Р', 'р', 'С', 'с', 'Т', 'т',
            'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч',
            'Ш', 'ш', 'Щ', 'щ',      'ъ',      'ы',      'ь',
            'Э', 'э', 'Ю', 'ю', 'Я', 'я'
        };

        public static ImitatorData GetData(string dbPath)
        {
            string meta = File.ReadAllText(Path.Combine(dbPath, "meta.dat"));

            (int Min, int Max, double Avg) interlineSize;
            (int Min, int Max, double Avg) spaceSize;

            try
            {
                string[] interlineData = meta.Split('\n')[0].Split('/');
                interlineSize = (int.Parse(interlineData[0]), int.Parse(interlineData[1]), double.Parse(interlineData[2]));

                string[] spaceData = meta.Split('\n')[1].Split('/');
                spaceSize = (int.Parse(spaceData[0]), int.Parse(spaceData[1]), double.Parse(spaceData[2]));
            }
            catch
            {
                throw new Exception("Структура файла с метаданными нарушена.");
            }

            List<Character> characters = new List<Character>();

            foreach (string file in Directory.GetFiles(dbPath, "*.bmp", SearchOption.TopDirectoryOnly))
            {
                string[] charData = Path.GetFileNameWithoutExtension(file).Split('.')[0..3];

                char label = char.Parse(charData[0]);
                if (!WorkingSet.Contains(label))
                {
                    throw new Exception("При чтении данных встретился необрабатываемый символ.");
                }

                char? leftNeighbour = charData[1][0];
                if (leftNeighbour == '_')
                {
                    leftNeighbour = null;
                }
                else if (!WorkingSet.Contains(((char)leftNeighbour).ToLower()))
                {
                    throw new Exception("При чтении данных встретился необрабатываемый символ.");
                }

                char? rightNeighbour = charData[1][1];
                if (rightNeighbour == '_')
                {
                    rightNeighbour = null;
                }
                else if (!WorkingSet.Contains(((char)rightNeighbour).ToLower()))
                {
                    throw new Exception("При чтении данных встретился необрабатываемый символ.");
                }

                bool[,] matrix = new Bitmap(file).ToGreyscaleMatrix().ToBoolMatrix();
                characters.Add(new Character(label, matrix, int.Parse(charData[2]), leftNeighbour, rightNeighbour));
            }

            return new ImitatorData(interlineSize, spaceSize, characters);
        }

        public static byte[,] Imitate(ImitatorData meta, string text)
        {
            string[] lines = text.Split('\n');
            //bool[,] textMatrix = new bool[0, 0];
            byte[,] textMatrix = new byte[0, 0];
            int interlineSpaceSum = GetInterline(meta.InterlineSize);

            foreach (string line in lines)
            {
                List<Character2> newLine = new List<Character2>();

                for (int i = 0; i < line.Length; i++)
                {
                    if (line[i] == ' ')
                    {
                        newLine.Add(GetSpace(meta.SpaceSize));
                    }
                    else
                    {
                        char? leftNeighbour = i != 0 && line[i - 1] != ' '
                            ? (char?)line[i - 1]
                            : null;

                        char? rightNeighbour = i != (line.Length - 1) && line[i + 1] != ' '
                            ? (char?)line[i + 1]
                            : null;

                        List<Character> characters = meta.FindSimilar(line[i], leftNeighbour, rightNeighbour);
                        newLine.Add(PickCharacterAndTransform(characters));
                    }
                }

                newLine.Insert(0, GetSpace(meta.SpaceSize));
                newLine.Add(GetSpace(meta.SpaceSize));

                // Суммарная ширина новой строки.
                int summaryWidth = newLine.Select(character => character.Matrix.Width()).Sum();

                // Максимальная вертикальный сдвиг новой строки относительно верхней границы.
                // Однако высота агрегирующего изображения может быть выше, если на предыдущих строках были высокие символы.
                int maxHeight = newLine.Select(character => character.Matrix.Height() + character.TopOffset + interlineSpaceSum).Max();

                // Изменение размера агрегирующего изображения и копирование в него уже сымитированных символов.
                byte[,] newTextMatrix = new byte[Math.Max(maxHeight, textMatrix.Height()), Math.Max(summaryWidth, textMatrix.Width())];
                newTextMatrix.Init((byte)255);
                textMatrix.InsertTo(newTextMatrix, 0, 0);
                textMatrix = newTextMatrix;

                // Помощение новых сымитированных символов в агрегирующее изображение.
                int j = 0;

                foreach (Character2 character in newLine)
                {
                    character.Matrix.InsertTo(textMatrix, interlineSpaceSum + character.TopOffset, j);
                    j += character.Matrix.Width();
                }

                // Добавление межстрочного интервала.
                interlineSpaceSum += GetInterline(meta.InterlineSize);
            }

            return textMatrix;
        }

        private static int GetInterline((int Min, int Max, double Avg) interlineSize)
        {
            return rand.Next(interlineSize.Min, interlineSize.Max + 1, interlineSize.Avg);
        }

        /*private static Character GetSpace((int Min, int Max, double Avg) spaceSize)
        {
            int spaceWidth = rand.Next(spaceSize.Min, spaceSize.Max + 1, spaceSize.Avg);
            bool[,] spaceMatrix = new bool[1, spaceWidth];
            spaceMatrix.Init(true);

            return new Character(' ', spaceMatrix, 0, null, null);
        }*/

        private static Character2 GetSpace((int Min, int Max, double Avg) spaceSize)
        {
            int spaceWidth = rand.Next(spaceSize.Min, spaceSize.Max + 1, spaceSize.Avg);
            byte[,] spaceMatrix = new byte[1, spaceWidth];
            spaceMatrix.Init((byte)255);

            return new Character2(' ', spaceMatrix, 0, null, null);
        }

        private static Character2 PickCharacterAndTransform(List<Character> characters)
        {
            if (characters.Count == 0) throw new ArgumentException(nameof(characters));

            int minHeight = characters.Min(c => c.Matrix.Height());
            int maxHeight = characters.Max(c => c.Matrix.Height());
            double avgHeight = characters.Average(c => c.Matrix.Height());

            int minWidth = characters.Min(c => c.Matrix.Width());
            int maxWidth = characters.Max(c => c.Matrix.Width());
            double avgWidth = characters.Average(c => c.Matrix.Width());

            int minOffset = characters.Min(c => c.TopOffset);
            int maxOffset = characters.Max(c => c.TopOffset);
            double avgOffset = characters.Average(c => c.TopOffset);

            int newHeight = rand.Next(minHeight, maxHeight + 1, avgHeight);
            int newWidth = rand.Next(minWidth, maxWidth + 1, avgWidth);
            int newOffset = rand.Next(minOffset, maxOffset + 1, avgOffset);

            Character character = characters[rand.Next(characters.Count)];
            character.Matrix = character.Matrix.Resize(newHeight, newWidth, false, true);
            character.TopOffset = newOffset;

            Character2 character2 = new Character2(character.Label, character.Matrix.ToByteMatrix(), character.TopOffset, character.LeftNeighbour, character.RightNeighbour);
            character2.Matrix = character2.Matrix.Resize(newHeight, newWidth, false);
            character2.TopOffset = newOffset;

            return character2;
        }
    }
}
