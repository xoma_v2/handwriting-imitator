﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imitator.Core
{
    public class Character
    {
        public char Label { get; set; }
        public bool[,] Matrix { get; set; }
        public int TopOffset { get; set; }
        public char? LeftNeighbour { get; set; }
        public char? RightNeighbour { get; set; }

        public Character(char label, bool[,] matrix, int topOffset, char? leftNeighbour, char? rightNeighbour)
        {
            Label = label;
            Matrix = matrix;
            TopOffset = topOffset;
            LeftNeighbour = leftNeighbour;
            RightNeighbour = rightNeighbour;
        }
    }

    public class Character2
    {
        public char Label { get; set; }
        public byte[,] Matrix { get; set; }
        public int TopOffset { get; set; }
        public char? LeftNeighbour { get; set; }
        public char? RightNeighbour { get; set; }

        public Character2(char label, byte[,] matrix, int topOffset, char? leftNeighbour, char? rightNeighbour)
        {
            Label = label;
            Matrix = matrix;
            TopOffset = topOffset;
            LeftNeighbour = leftNeighbour;
            RightNeighbour = rightNeighbour;
        }
    }
}
