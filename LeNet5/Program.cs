using LeNet5.Models;
using LeNet5.Models.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeNet5
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // csv-���� ��������� �� (28 * 28 + 1) �������.
            // ������ ������� - ������ ������, � �������� ����������� ������.
            // ��� ��������� ������� - ������������� ������� �� 0 �� 255.
            string csvPath = "Data/HRCC.balanced.binary.csv";

            List<char> characters0 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            LeNetTrainer trainer0 = new LeNetTrainer(characters0, csvPath);
            trainer0.Train("Training/0");

            List<char> characters1 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            LeNetTrainer trainer1 = new LeNetTrainer(characters1, csvPath);
            trainer1.Train("Training/1", false);

            List<char> characters2 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            LeNetTrainer trainer2 = new LeNetTrainer(characters2, csvPath);
            trainer2.Train("Training/2", false);

            //List<char> characters3 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            //LeNetTrainer trainer3 = new LeNetTrainer(characters3, csvPath);
            //trainer3.Train("Training/3", false);

            //List<char> characters4 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            //LeNetTrainer trainer4 = new LeNetTrainer(characters4, csvPath);
            //trainer4.Train("Training/4", false);

            //List<char> characters5 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            //LeNetTrainer trainer5 = new LeNetTrainer(characters5, csvPath);
            //trainer5.Train("Training/5", false);

            //List<char> characters6 = new List<char> { '�', '�', '�', '�', '�', '�', '�', '�' };
            //LeNetTrainer trainer6 = new LeNetTrainer(characters6, csvPath);
            //trainer6.Train("Training/6", false);

            Application.Run(new LeNetObservationForm(trainer0.Snapshot));
        }
    }
}
