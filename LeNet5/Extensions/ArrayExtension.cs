﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeNet5.Extensions
{
    public static class ArrayExtension
    {
        public static void Init<T>(this T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = value;
            }
        }
    }
}
