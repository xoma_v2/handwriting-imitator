﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace LeNet5.Extensions
{
    public static class DoubleArrayExtension
    {
        public static Bitmap ToBitmap(this double[] doubles, int width)
        {
            if (width <= 0) throw new ArgumentException();

            int height = (doubles.Length + width - 1) / width;
            Bitmap image = new Bitmap(width, height, PixelFormat.Format32bppPArgb);

            for (int i = 0; i < doubles.Length; i++)
            {
                image.SetPixel(i % width, i / width, doubles[i].ToPixel());
            }

            return image;
        }

        private static Color ToPixel(this double value)
        {
            double boundedValue = Math.Min(Math.Max(value + 2, 0), 4);
            byte pixelState = (byte)(boundedValue * 255 / 4);

            return Color.FromArgb(255, pixelState, pixelState, pixelState);
        }
    }
}
