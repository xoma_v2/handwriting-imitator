﻿using LeNet5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeNet5.Extensions
{
    public enum BatchMode
    {
        OneClassPerBatch,
        EachClassPerBatch,
        JustInOrder
    }

    public static class DatasetItemArrayExtension
    {
        public static DatasetItem[] ReduceDataset(this DatasetItem[] images, double fraction = 0.5)
        {
            var labelCount = (from image in images
                              group image by image.Label into g
                              select new { Label = g.Key, Count = (int)(g.Count() * fraction) })
                              .ToDictionary(lc => lc.Label, lc => lc.Count);

            List<DatasetItem> imagesReduced = new List<DatasetItem>();

            foreach (DatasetItem image in images)
            {
                if (labelCount[image.Label] != 0)
                {
                    labelCount[image.Label]--;
                    imagesReduced.Add(image);
                }
            }

            return imagesReduced.ToArray();
        }

        public static DatasetItem[] GroupByLabelFlat(this DatasetItem[] images, int? skipPerClass = null, int? takePerClass = null)
        {
            var orderedLabels = from image in images
                                group image by image.Label into g
                                select g.Key;

            List<DatasetItem> imagesGrouped = new List<DatasetItem>();

            foreach (var label in orderedLabels)
            {
                imagesGrouped.AddRange((from image in images
                                        where image.Label == label
                                        select image)
                                        .Skip(skipPerClass ?? 0)
                                        .Take(takePerClass ?? int.MaxValue));
            }

            return imagesGrouped.ToArray();
        }

        public static int GetClassCount(this DatasetItem[] images)
        {
            return (from image in images
                    group image by image.Label into g
                    select g.Key)
                    .Count();
        }

        public static (DatasetItem[] TrainImages, DatasetItem[] TestImages) SplitDataset(this DatasetItem[] images, double fraction = 0.2)
        {
            var trainCount = (from image in images
                              group image by image.Label into g
                              select new { Label = g.Key, TrainCount = (int)(g.Count() * (1 - fraction)) })
                              .ToDictionary(tc => tc.Label, tc => tc.TrainCount);

            List<DatasetItem> imagesTrain = new List<DatasetItem>();
            List<DatasetItem> imagesTest = new List<DatasetItem>();

            foreach (var image in images)
            {
                if (trainCount[image.Label] != 0)
                {
                    trainCount[image.Label]--;
                    imagesTrain.Add(image);
                }
                else
                {
                    imagesTest.Add(image);
                }
            }

            return (imagesTrain.ToArray(), imagesTest.ToArray());
        }

        public static List<DatasetItem[]> GetBatches(this DatasetItem[] images, BatchMode mode, int? batchSize = null)
        {
            List<DatasetItem[]> batches = new List<DatasetItem[]>();

            switch (mode)
            {
                case BatchMode.JustInOrder:
                    List<DatasetItem> batchList = new List<DatasetItem>();
                    int currentBatchSize = batchSize ?? images.Length;

                    foreach (DatasetItem image in images)
                    {
                        if (currentBatchSize == 0)
                        {
                            batches.Add(batchList.ToArray());
                            batchList.Clear();
                            currentBatchSize = batchSize ?? images.Length;
                        }

                        currentBatchSize--;
                        batchList.Add(image);
                    }

                    batches.Add(batchList.ToArray());
                    break;

                case BatchMode.EachClassPerBatch:
                    DatasetItem[] batchArray = GroupByLabelFlat(images, 0, batchSize ?? images.Length);
                    int skipIndex = 1;

                    while (batchArray.Length != 0)
                    {
                        batches.Add(batchArray);
                        batchArray = GroupByLabelFlat(images, skipIndex * (batchSize ?? images.Length), batchSize ?? images.Length);
                        skipIndex++;
                    }

                    break;

                case BatchMode.OneClassPerBatch:
                    var orderedLabels = (from image in images
                                         group image by image.Label into g
                                         select g.Key)
                                         .ToArray();
                    var orderedCounts = (from image in images
                                         group image by image.Label into g
                                         select g.Count())
                                         .ToArray();

                    while (batches.Select(b => b.Length).Sum() != images.Length)
                    {
                        for (int i = 0; i < orderedCounts.Length; i++)
                        {
                            if (orderedCounts[i] > 0)
                            {
                                batches.Add((from image in images
                                             where image.Label == orderedLabels[i]
                                             select image)
                                             .Skip(i * (batchSize ?? orderedCounts[i]))
                                             .Take(batchSize ?? orderedCounts[i])
                                             .ToArray());

                                orderedCounts[i] -= batchSize ?? orderedCounts[i];
                            }
                        }
                    }

                    break;
            }

            return batches;
        }
    }
}
