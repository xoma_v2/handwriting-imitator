﻿using System.Drawing;

namespace LeNet5.Extensions
{
    public static class BitmapExtension
    {
        public static double[] ToDoubles(this Bitmap bitmap)
        {
            int width = bitmap.Width;
            int height = bitmap.Height;
            double[] doubles = new double[width * height];

            for (int i = 0; i < doubles.Length; i++)
            {
                doubles[i] = 1 - 2 * bitmap.GetPixel(i % width, i / width).GetBrightness();
            }

            return doubles;
        }
    }
}
