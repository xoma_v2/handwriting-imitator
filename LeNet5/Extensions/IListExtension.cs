﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeNet5.Extensions
{
    public static class IListExtension
    {
        public static IList<T> Shuffle<T>(this IList<T> items)
        {
            return items.Shuffle(new Random());
        }

        public static IList<T> Shuffle<T>(this IList<T> items, int randSeed)
        {
            return items.Shuffle(new Random(randSeed));
        }

        public static IList<T> Shuffle<T>(this IList<T> items, Random random)
        {
            List<T> result = new List<T>(items);

            for (int i = 0; i < result.Count; i++)
            {
                int j = random.Next(result.Count);

                T item = result[i];
                result[i] = result[j];
                result[j] = item;
            }

            return result;
        }
    }
}
