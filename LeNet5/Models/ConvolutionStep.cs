﻿using LeNet5.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models
{
    [Serializable]
    class ConvolutionStep : RectangularStep
    {
        private readonly ConvolutionWeights weights;
        public override Weights Weights => weights;

        public ConvolutionStep(RectangularStep upstream, int convolutionSize, int randSeed)
            : this(new[] { upstream }, convolutionSize, convolutionSize, randSeed)
        { }

        public ConvolutionStep(RectangularStep upstream, int convolutionWidth, int convolutionHeight, int randSeed)
            : this(new[] { upstream }, convolutionWidth, convolutionHeight, randSeed)
        { }

        public ConvolutionStep(IList<RectangularStep> upstream, int convolutionSize, int randSeed)
            : this(upstream, convolutionSize, convolutionSize, randSeed)
        { }

        public ConvolutionStep(IList<RectangularStep> upstream, int convolutionWidth, int convolutionHeight, int randSeed)
            : base(WidthOf(upstream) - convolutionWidth + 1, HeightOf(upstream) - convolutionHeight + 1, upstream)
        {
            weights = new ConvolutionWeights(convolutionWidth, convolutionHeight, upstream.Count, randSeed);
        }
    }
}
