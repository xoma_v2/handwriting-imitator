﻿using LeNet5.Models.Abstract;
using LeNet5.Models.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models
{
    [Serializable]
    class MarkingStep : Step
    {
        protected readonly MarkingWeights weights;

        public override Weights Weights => weights;

        public MarkingStep(Step upstream, LeNetConfiguration configuration, int randSeed)
            : this(new[] { upstream }, configuration, randSeed)
        { }

        public MarkingStep(IList<Step> upstream, LeNetConfiguration configuration, int randSeed)
            : base(configuration.ClassCount, upstream, true)
        {
            if (SizeOf(upstream) * upstream.Count != LeNetNetwork.L6OutputCount) throw new ArgumentException();

            weights = new MarkingWeights(configuration, randSeed);
        }

        public int CorrectClass
        {
            get => weights.CorrectClass;
            set => weights.CorrectClass = value;
        }

        public override double CalculateActivation(double weightedInputs)
        {
            double coshx = Math.Cosh(2 / 3.0 * weightedInputs);
            double denominator = Math.Cosh(4 / 3.0 * weightedInputs) + 1;

            return 4.57573 * coshx * coshx / (denominator * denominator);
        }

        public override double CalculateActivationDerivative(double weightedInputs)
        {
            double coshTwoThirdsx = Math.Cosh(2 / 3.0 * weightedInputs);
            double sinhFourThirdsx = Math.Sinh(4 / 3.0 * weightedInputs);
            double denominator = Math.Cosh(4 / 3.0 * weightedInputs) + 1;
            denominator = denominator * denominator * denominator;

            return -6.10098 * sinhFourThirdsx * coshTwoThirdsx * coshTwoThirdsx / denominator;
        }

    }
}
