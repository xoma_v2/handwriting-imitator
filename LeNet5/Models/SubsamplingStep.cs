﻿using LeNet5.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models
{
    [Serializable]
    class SubsamplingStep : RectangularStep
    {
        public SubsamplingStep(RectangularStep upstream, int subsamplingSize, int randSeed)
            : this(new[] { upstream }, subsamplingSize, randSeed)
        { }


        public SubsamplingStep(RectangularStep upstream, int subsamplingWidth, int subsamplingHeight, int randSeed)
            : this(new[] { upstream }, subsamplingWidth, subsamplingHeight, randSeed)
        { }

        public SubsamplingStep(IList<RectangularStep> upstream, int subsamplingSize, int randSeed)
            : this(upstream, subsamplingSize, subsamplingSize, randSeed)
        { }

        public SubsamplingStep(IList<RectangularStep> upstream, int subsamplingWidth, int subsamplingHeight, int randSeed)
             : base(WidthOf(upstream) / subsamplingWidth,
                 HeightOf(upstream) / subsamplingHeight, upstream)
        {
            if (WidthOf(upstream) % subsamplingWidth != 0) throw new ArgumentException();
            if (HeightOf(upstream) % subsamplingHeight != 0) throw new ArgumentException();

            weights = new SubsamplingWeights(subsamplingWidth, subsamplingHeight, randSeed);
        }

        private readonly SubsamplingWeights weights;
        public override Weights Weights => weights;
    }
}
