﻿using LeNet5.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models
{
    [Serializable]
    class FeedForwardStep : Step
    {
        public FeedForwardStep(int outputs, int randSeed, params Step[] upstream)
            : this(outputs, randSeed, (IList<Step>)upstream)
        { }

        public FeedForwardStep(int outputs, int randSeed, IList<Step> upstream)
            : base(outputs, upstream)
        {
            weights = new FeedForwardWeights(SizeOf(upstream) * upstream.Count, outputs, randSeed);
        }

        private readonly FeedForwardWeights weights;

        public override Weights Weights => weights;
    }
}
