﻿using LeNet5.Extensions;
using LeNet5.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models
{
    class StepSnapshot
    {
        public readonly Step Step;
        public readonly int Width;
        public readonly double[] OutputSnapshot;
        public Bitmap OutputBitmap;
        public event EventHandler Updated;

        public StepSnapshot(RectangularStep step) : this(step, step.Width)
        { }

        public StepSnapshot(Step step, int width)
        {
            Step = step;
            OutputSnapshot = new double[step.Output.Length];
            Width = width;
        }

        public void UpdateOutputBitmap()
        {
            OutputBitmap = OutputSnapshot.ToBitmap(Width);
        }

        protected void OnUpdated() => Updated?.Invoke(this, EventArgs.Empty);

        public virtual void UpdateSnapshot()
        {
            Array.Copy(Step.Output, OutputSnapshot, OutputSnapshot.Length);
            OnUpdated();
        }
    }
}
