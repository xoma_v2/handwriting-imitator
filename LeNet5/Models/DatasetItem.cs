﻿using LeNet5.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models
{
    public struct DatasetItem
    {
        public double[] Inputs;
        public char Label;

        public static Dictionary<byte, char> CharactersMapping = new Dictionary<byte, char>
        {
            { 0,  'А' }, { 1,  'а' }, { 2,  'Б' }, { 3,  'б' },
            { 4,  'В' }, { 5,  'в' }, { 6,  'Г' }, { 7,  'г' },
            { 8,  'Д' }, { 9,  'д' }, { 10, 'Е' }, { 11, 'е' },
            { 12, 'Ё' }, { 13, 'ё' }, { 14, 'Ж' }, { 15, 'ж' },
            { 16, 'З' }, { 17, 'з' }, { 18, 'И' }, { 19, 'и' },
                         { 20, 'й' }, { 21, 'К' }, { 22, 'к' },
            { 23, 'Л' }, { 24, 'л' }, { 25, 'М' }, { 26, 'м' },
            { 27, 'Н' }, { 28, 'н' }, { 29, 'О' }, { 30, 'о' },
            { 31, 'П' }, { 32, 'п' }, { 33, 'Р' }, { 34, 'р' },
            { 35, 'С' }, { 36, 'с' }, { 37, 'Т' }, { 38, 'т' },
            { 39, 'У' }, { 40, 'у' }, { 41, 'Ф' }, { 42, 'ф' },
            { 43, 'Х' }, { 44, 'х' }, { 45, 'Ц' }, { 46, 'ц' },
            { 47, 'Ч' }, { 48, 'ч' }, { 49, 'Ш' }, { 50, 'ш' },
            { 51, 'Щ' }, { 52, 'щ' },
            { 53, 'ъ' }, { 54, 'ы' }, { 55, 'ь' },
            { 56, 'Э' }, { 57, 'э' }, { 58, 'Ю' }, { 59, 'ю' },
            { 60, 'Я' }, { 61, 'я' }
        };

        public static DatasetItem[] ReadCsv(string inputPath, List<char> selectedCharacters = null)
        {
            if (selectedCharacters != null
                && (selectedCharacters.Count > 62
                    || selectedCharacters.Count == 0
                    || selectedCharacters.Distinct().Count() != selectedCharacters.Count()))
            {
                throw new ArgumentException(nameof(selectedCharacters));
            }

            List<DatasetItem> items = new List<DatasetItem>();
            object sync = new object();

            Parallel.ForEach(File.ReadLines(inputPath), line =>
            {
                string[] values = line.Split(',');
                char label = CharactersMapping[byte.Parse(values[0])];

                if (selectedCharacters != null)
                {
                    int index = selectedCharacters.FindIndex(c => c == label);

                    if (index == -1)
                    {
                        return;
                    }
                }

                double[] inputs = new double[32 * 32];
                inputs.Init(-0.1);
                int valueIndex = 1;
                int inputIndex = 32 + 32 + 2;

                for (int i = 2; i < 30; i++)
                {
                    for (int j = 2; j < 30; j++)
                    {
                        // Background pixel is -0.1 (black) and Foreground is 1.175. 
                        // Refer to page 7 of LeCun's document on Gradient-based learning applied to document recognition.
                        inputs[inputIndex++] = byte.Parse(values[valueIndex++]) / 255.0 * 1.275 - 0.1;
                    }

                    inputIndex += 4;
                }

                DatasetItem item = new DatasetItem { Label = label, Inputs = inputs };

                lock (sync)
                {
                    items.Add(item);
                }
            });

            return items.ToArray();
        }
    }
}
