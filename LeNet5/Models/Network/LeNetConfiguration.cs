﻿using LeNet5.Extensions;
using System;
using System.Drawing;
using System.Linq;

namespace LeNet5.Models.Network
{
    [Serializable]
    class LeNetConfiguration
    {
        public readonly int ClassCount;
        public readonly double[] Definitions;
        public readonly char[] Characters;

        public LeNetConfiguration(params char[] characters)
        {
            if (characters.Length != characters.Distinct().Count()) throw new ArgumentException();

            ClassCount = characters.Length;
            Characters = new char[characters.Length];
            Definitions = new double[characters.Length * LeNetNetwork.OutputWidth * LeNetNetwork.OutputHeight];
            Array.Copy(characters, Characters, characters.Length);

            for (int i = 0; i < characters.Length; i++)
            {
                double[] definition = GetCharacterDefinition(characters[i]);
                Array.Copy(definition, 0, Definitions, i * LeNetNetwork.OutputWidth * LeNetNetwork.OutputHeight, definition.Length);
            }
        }

        private double[] GetCharacterDefinition(char character)
        {
            Bitmap image = new Bitmap(LeNetNetwork.OutputWidth, LeNetNetwork.OutputHeight);
            Font characterFont = new Font("Lucida Console", 13, FontStyle.Bold, GraphicsUnit.Pixel);

            using (Graphics graphics = Graphics.FromImage(image))
            {
                graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                graphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, image.Width, image.Height));
                graphics.DrawString($"{character}", characterFont, new SolidBrush(Color.Black), -LeNetNetwork.OutputWidth / 2, 0);
            }

            return image.ToDoubles();
        }
    }
}
