﻿using LeNet5.Models.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models.Network
{
    class LeNetSnapshot
    {
        public readonly LeNetNetwork Network;
        public StepSnapshot Input { get; protected set; }
        public StepSnapshot[] FirstConvolutions { get; protected set; }
        public StepSnapshot[] FirstSubsampling { get; protected set; }
        public StepSnapshot[] SecondConvolutions { get; protected set; }
        public StepSnapshot[] SecondSubsampling { get; protected set; }
        public StepSnapshot Consolidation { get; protected set; }
        public StepSnapshot Output { get; protected set; }
        public StepSnapshot Marking { get; protected set; }
        public bool UpdateRequested { get; protected set; }
        public event EventHandler Updated;

        public LeNetSnapshot(LeNetNetwork network)
        {
            Network = network;
            Input = new StepSnapshot(network.L0Input);
            FirstConvolutions = network.L1Convolutions.Select(step => new StepSnapshot(step)).ToArray();
            FirstSubsampling = network.L2Subsampling.Select(step => new StepSnapshot(step)).ToArray();
            SecondConvolutions = network.L3Convolutions.Select(step => new StepSnapshot(step)).ToArray();
            SecondSubsampling = network.L4Subsampling.Select(step => new StepSnapshot(step)).ToArray();
            Consolidation = new StepSnapshot(network.L5Consolidation, 1);
            Output = new StepSnapshot(network.L6Output, LeNetNetwork.OutputWidth);
            Marking = new StepSnapshot(network.L7Marking, network.L7Marking.Length);
        }

        protected IEnumerable<StepSnapshot> All()
        {
            yield return Input;

            foreach (StepSnapshot snapshot in FirstConvolutions)
            {
                yield return snapshot;
            }

            foreach (StepSnapshot snapshot in FirstSubsampling)
            {
                yield return snapshot;
            }

            foreach (StepSnapshot snapshot in SecondConvolutions)
            {
                yield return snapshot;
            }

            foreach (StepSnapshot snapshot in SecondSubsampling)
            {
                yield return snapshot;
            }

            yield return Consolidation;
            yield return Output;
            yield return Marking;
        }

        public void RequestUpdate()
        {
            if (!UpdateRequested)
            {
                UpdateRequested = true;
            }
        }

        protected void OnUpdated()
        {
            UpdateRequested = false;
            Updated?.Invoke(this, EventArgs.Empty);
        }

        public void UpdateSnapshot()
        {
            if (UpdateRequested)
            {
                foreach (StepSnapshot snapshot in All())
                {
                    snapshot.UpdateSnapshot();
                }

                Task.Run(new Action(UpdateOutputBitmaps));
            }
        }

        protected void UpdateOutputBitmaps()
        {
            foreach (StepSnapshot snapshot in All())
            {
                snapshot.UpdateOutputBitmap();
            }

            OnUpdated();
        }
    }
}
