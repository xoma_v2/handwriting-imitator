﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace LeNet5.Models.Network
{
    public class LeNetRecognizer
    {
        private LeNetNetwork[] models;

        public LeNetRecognizer(params string[] modelPaths)
        {
            modelPaths = modelPaths.Distinct().ToArray();
            models = new LeNetNetwork[modelPaths.Length];

            for (int i = 0; i < modelPaths.Length; i++)
            {
                using (var fs = File.OpenRead(modelPaths[i]))
                {
                    var bf = new BinaryFormatter();
                    models[i] = (LeNetNetwork)bf.Deserialize(fs);
                }
            }
        }

        public (bool IsCorrect, double Error) Recognize(byte[,] image, char label)
        {
            if (image.Length != 32 * 32) throw new ArgumentException();

            LeNetNetwork selectedModel = null;

            foreach (var model in models)
            {
                if (model.Configuration.Characters.Contains(label))
                {
                    selectedModel = model;
                    break;
                }
            }

            if (selectedModel == null)
            {
                return (false, double.MaxValue);
            }

            double[] inputs = new double[32 * 32];
            int k = 0;

            foreach (byte pixel in image)
            {
                // Background pixel is -0.1 (black) and Foreground is 1.175. 
                // Refer to page 7 of LeCun's document on Gradient-based learning applied to document recognition.
                inputs[k++] = pixel / 255.0 * 1.275 - 0.1;
            }

            DatasetItem item = new DatasetItem { Inputs = inputs, Label = label };
            selectedModel.IsPreTraining = true;
            TrainingResults result = selectedModel.Train(item);

            return (result.IsCorrect, result.Error);
        }
    }
}
