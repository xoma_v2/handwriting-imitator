﻿using LeNet5.Models.Abstract;
using LeNet5.Models.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models.Network
{
    [Serializable]
    class LeNetNetwork
    {
        public InputStep L0Input { get; protected set; }

        public ConvolutionStep[] L1Convolutions { get; protected set; }
        private const int L1ConvolutionCount = 6;
        private const int L1ConvolutionSize = 5;

        public SubsamplingStep[] L2Subsampling { get; protected set; }

        public ConvolutionStep[] L3Convolutions { get; protected set; }
        private const int L3ConvolutionCount = 16;
        private const int L3ConvolutionSize = 5;
        private readonly bool[,] L3ConvolutionConnections = new bool[16, 6]
        {
            { true,  true,  true,  false, false, false }, 
            { false, true,  true,  true,  false, false },
            { false, false, true,  true,  true,  false },
            { false, false, false, true,  true,  true  },
            { true,  false, false, false, true,  true  },
            { true,  true,  false, false, false, true  },

            { true,  true,  true,  true,  false, false },
            { false, true,  true,  true,  true,  false },
            { false, false, true,  true,  true,  true  },
            { true,  false, false, true,  true,  true  },
            { true,  true,  false, false, true,  true  },
            { true,  true,  true,  false, false, true  },

            { true,  true,  false, true,  true,  false },
            { false, true,  true,  false, true,  true  },
            { true,  false, true,  true,  false, true  },

            { true,  true,  true,  true,  true,  true  }
        };

        public SubsamplingStep[] L4Subsampling { get; protected set; }

        public FeedForwardStep L5Consolidation { get; protected set; }
        public const int L5ConsolidationCount = 120;

        public FeedForwardStep L6Output { get; protected set; }
        public const int OutputWidth = 7;
        public const int OutputHeight = 12;
        public static int L6OutputCount => OutputWidth * OutputHeight;

        public MarkingStep L7Marking { get; protected set; }

        protected Step[] ForwardSteps;
        protected Step[] ReverseSteps;

        public LeNetConfiguration Configuration;

        private bool preTraining;

        public LeNetNetwork(int randSeed, params char[] characters)
        {
            Weights.GlobalLearningRate = 0.00001;
            Weights.GlobalMu = 0.02;
            Configuration = new LeNetConfiguration(characters);
            CreateNetwork(randSeed);
        }

        private void CreateNetwork(int randSeed)
        {
            InstanciateSteps(randSeed);
            CreateStepLists();
        }

        private void InstanciateSteps(int randSeed)
        {
            L0Input = new InputStep(32, 32);
            L1Convolutions = new ConvolutionStep[L1ConvolutionCount];
            L2Subsampling = new SubsamplingStep[L1ConvolutionCount];

            for (int i = 0; i < L1ConvolutionCount; i++)
            {
                L1Convolutions[i] = new ConvolutionStep(L0Input, L1ConvolutionSize, randSeed);
                L2Subsampling[i] = new SubsamplingStep(L1Convolutions[i], 2, randSeed);
            }

            L3Convolutions = new ConvolutionStep[L3ConvolutionCount];
            L4Subsampling = new SubsamplingStep[L3ConvolutionCount];

            for (int i = 0; i < L3ConvolutionCount; i++)
            {
                RectangularStep[] inputs = L2Subsampling.Where((item, upstreamIndex) => L3ConvolutionConnections[i, upstreamIndex]).ToArray();

                L3Convolutions[i] = new ConvolutionStep(inputs, L3ConvolutionSize, randSeed);
                L4Subsampling[i] = new SubsamplingStep(L3Convolutions[i], 2, randSeed);
            }

            L5Consolidation = new FeedForwardStep(L5ConsolidationCount, randSeed, L4Subsampling);
            L6Output = new FeedForwardStep(L6OutputCount, randSeed, L5Consolidation);
            L7Marking = new MarkingStep(L6Output, Configuration, randSeed);
        }

        private void CreateStepLists()
        {
            List<Step> steps = new List<Step>();
            
            steps.AddRange(L1Convolutions);
            steps.AddRange(L2Subsampling);
            steps.AddRange(L3Convolutions);
            steps.AddRange(L4Subsampling);
            steps.Add(L5Consolidation);
            steps.Add(L6Output);
            steps.Add(L7Marking);

            ForwardSteps = steps.ToArray();
            steps.Reverse();
            ReverseSteps = steps.ToArray();
        }

        public bool IsPreTraining
        {
            get => preTraining;
            set
            {
                if (preTraining != value)
                {
                    preTraining = value;
                    Array.ForEach(ForwardSteps, step => step.IsPreTraining = preTraining);
                }
            }
        }

        public void PropogateForward(DatasetItem inputs)
        {
            L0Input.SetInputs(inputs.Inputs);
            Array.ForEach(ForwardSteps, step => step.PropogateForward());
        }

        public TrainingResults Train(DatasetItem inputs)
        {
            int correctOutputIndex = Array.IndexOf(Configuration.Characters, inputs.Label);

            PropogateForward(inputs);    
            L7Marking.CorrectClass = correctOutputIndex;
            Array.ForEach(ReverseSteps, step => step.PropogateBackwards());

            return new TrainingResults(L7Marking.Output, correctOutputIndex);
        }
    }

    class TrainingResults
    {
        public readonly double Error;
        public readonly bool IsCorrect;

        public TrainingResults(double[] results, int correctClass)
        {
            Error = results[correctClass];
            IsCorrect = Error < results.Where((result, index) => index != correctClass).Min();
        }
    }
}
