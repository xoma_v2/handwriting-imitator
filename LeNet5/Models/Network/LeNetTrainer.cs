﻿using LeNet5.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LeNet5.Models.Network
{
    class LeNetTrainer
    {
        public LeNetSnapshot Snapshot { get; private set; }
        
        private LeNetNetwork network;
        private DatasetItem[] trainingDataset;
        private DatasetItem[] testDataset;

        public LeNetTrainer(List<char> characters, string csvPath, int randSeed = 1)
        {
            DatasetItem[] items = DatasetItem.ReadCsv(csvPath, characters);
            items.Shuffle(randSeed);
            (trainingDataset, testDataset) = items.SplitDataset(0.1);
            network = new LeNetNetwork(randSeed, characters.ToArray());
            Snapshot = new LeNetSnapshot(network);
        }

        public async Task Train(string? outputDir = null, bool showStat = true)
        {
            await Task.Run(() =>
            {
                if (showStat) Console.WriteLine();

                for (int i = 0; i < 50; i++)
                {
                    if (showStat) Console.WriteLine($"Run Epoch {i}");

                    network.IsPreTraining = true;
                    DoEpoch(testDataset, showStat);

                    network.IsPreTraining = false;
                    var result = DoEpoch(trainingDataset, showStat);

                    if (outputDir != null)
                    {
                        Directory.CreateDirectory(outputDir);
                        double error = result.ErrorSum / result.TotalCount;
                        double accuracy = result.CorrectCount * 100.0 / result.TotalCount;

                        using (var fs = File.Create($"{outputDir}/e{error:00.00}_a{accuracy:00.00}.dat"))
                        {
                            var bf = new BinaryFormatter();
                            bf.Serialize(fs, network);
                        }
                    }
                }

                if (showStat) Console.WriteLine("Complete.");
            });
        }

        private (int CorrectCount, int TotalCount, double ErrorSum) DoEpoch(IEnumerable<DatasetItem> trainItems, bool showStat = true)
        {
            int correct = 0;
            int total = 0;
            double errorSum = 0;

            foreach (DatasetItem item in trainItems)
            {
                TrainingResults result = network.Train(item);
                correct += result.IsCorrect ? 1 : 0;
                total++;
                errorSum += result.Error;

                if (Snapshot.UpdateRequested)
                {
                    Snapshot.UpdateSnapshot();
                }

                if (total % 10 == 0)
                {
                    if (showStat) UpdateStatus(correct, total, errorSum);
                }
            }

            if (showStat) Console.WriteLine();

            return (correct, total, errorSum);
        }

        private void UpdateStatus(int itemsCorrect, int itemsProcessed, double errorSum)
        {
            Console.CursorLeft = 0;
            Console.Write($"Error: {(errorSum / itemsProcessed):00.00}; accuracy: {(itemsCorrect * 100.0 / itemsProcessed):00.00}% on {itemsProcessed} items...");
        }
    }
}
