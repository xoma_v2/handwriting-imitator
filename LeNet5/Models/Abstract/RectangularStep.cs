﻿using LeNet5.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeNet5.Models.Abstract
{
    [Serializable]
    abstract class RectangularStep : Step
    {
        public readonly int Width;
        public readonly int Height;
        public new readonly ReadOnlyCollection<RectangularStep> Upstream;

        public RectangularStep(int width, int height, IList<RectangularStep> upstream)
            : base(width * height, upstream == null ? null : upstream.ToList<Step>())
        {
            if (width <= 0 || height <= 0) throw new ArgumentException();

            Width = width;
            Height = height;
            Upstream = upstream != null
                ? new ReadOnlyCollection<RectangularStep>(upstream)
                : null;
        }

        protected static int WidthOf(IList<RectangularStep> upstream)
        {
            if (upstream == null || upstream.Count == 0) throw new ArgumentException();

            int width = upstream[0].Width;

            if (!upstream.All(step => step.Width == width)) throw new ArgumentException();

            return width;
        }

        protected static int HeightOf(IList<RectangularStep> upstream)
        {
            if (upstream == null || upstream.Count == 0) throw new ArgumentException();

            int height = upstream[0].Height;

            if (!upstream.All(step => step.Height == height)) throw new ArgumentException();

            return height;
        }
    }
}
