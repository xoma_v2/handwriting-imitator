﻿using System;
using System.Diagnostics;

namespace LeNet5.Models.Abstract
{
    [Serializable]
    abstract class Weights
    {
        public readonly int Size;
        public static double GlobalMu;
        public static double GlobalLearningRate;
        protected static Random Rand;
        protected int PreTrainingSamples { get; private set; }

        protected Weights(int size, int randSeed)
        {
            Size = size;
            Rand = new Random(randSeed);
        }

        public void StartPreTraining()
        {
            PreTrainingSamples = 0;
            StartPreTrainingCore();
        }

        protected abstract void StartPreTrainingCore();

        public virtual void PreTrain(Step step)
        {
            PreTrainingSamples += 1;
            PreTrainCore(step);
        }

        protected abstract void PreTrainCore(Step downstream);

        public void CompletePreTraining()
        {
            Debug.Assert(PreTrainingSamples != 0);
            CompletePreTrainingCore();
        }

        protected abstract void CompletePreTrainingCore();

        public virtual void ProprogateForward(Step downstream)
        {
            PropogateForwardCore(downstream);
        }

        protected abstract void PropogateForwardCore(Step step);

        public virtual void Train(Step downstream)
        {
            TrainCore(downstream);
        }

        protected abstract void TrainCore(Step downstream);

        protected static void Randomise(double[] weights, int fanIn)
        {
            for (int i = 0; i < weights.Length; i++)
            {
                weights[i] = (Rand.NextDouble() - 0.5) * 4.8 / fanIn;
            }
        }

        protected static double RandomWeight(int fanIn)
        {
            return (Rand.NextDouble() - 0.5) * 4.8 / fanIn;
        }
    }
}
