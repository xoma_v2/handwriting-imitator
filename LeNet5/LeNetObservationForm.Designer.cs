﻿namespace LeNet5
{
    partial class LeNetObservationForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputPicture = new System.Windows.Forms.PictureBox();
            this.consolidationPicture = new System.Windows.Forms.PictureBox();
            this.outputPicture = new System.Windows.Forms.PictureBox();
            this.firstConvolutionsContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.firstSubsamplingContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.secondConvolutionsContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.secondSubsamplingContainer = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.inputPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.consolidationPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // inputPicture
            // 
            this.inputPicture.Location = new System.Drawing.Point(9, 20);
            this.inputPicture.Margin = new System.Windows.Forms.Padding(2);
            this.inputPicture.Name = "inputPicture";
            this.inputPicture.Size = new System.Drawing.Size(96, 96);
            this.inputPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.inputPicture.TabIndex = 0;
            this.inputPicture.TabStop = false;
            // 
            // consolidationPicture
            // 
            this.consolidationPicture.Location = new System.Drawing.Point(973, 20);
            this.consolidationPicture.Margin = new System.Windows.Forms.Padding(2);
            this.consolidationPicture.Name = "consolidationPicture";
            this.consolidationPicture.Size = new System.Drawing.Size(15, 585);
            this.consolidationPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.consolidationPicture.TabIndex = 5;
            this.consolidationPicture.TabStop = false;
            // 
            // outputPicture
            // 
            this.outputPicture.Location = new System.Drawing.Point(992, 20);
            this.outputPicture.Margin = new System.Windows.Forms.Padding(2);
            this.outputPicture.Name = "outputPicture";
            this.outputPicture.Size = new System.Drawing.Size(21, 36);
            this.outputPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.outputPicture.TabIndex = 6;
            this.outputPicture.TabStop = false;
            // 
            // firstConvolutionsContainer
            // 
            this.firstConvolutionsContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.firstConvolutionsContainer.Location = new System.Drawing.Point(119, 11);
            this.firstConvolutionsContainer.Margin = new System.Windows.Forms.Padding(2);
            this.firstConvolutionsContainer.Name = "firstConvolutionsContainer";
            this.firstConvolutionsContainer.Size = new System.Drawing.Size(312, 631);
            this.firstConvolutionsContainer.TabIndex = 7;
            // 
            // firstSubsamplingContainer
            // 
            this.firstSubsamplingContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.firstSubsamplingContainer.Location = new System.Drawing.Point(435, 11);
            this.firstSubsamplingContainer.Margin = new System.Windows.Forms.Padding(2);
            this.firstSubsamplingContainer.Name = "firstSubsamplingContainer";
            this.firstSubsamplingContainer.Size = new System.Drawing.Size(186, 631);
            this.firstSubsamplingContainer.TabIndex = 8;
            // 
            // secondConvolutionsContainer
            // 
            this.secondConvolutionsContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.secondConvolutionsContainer.Location = new System.Drawing.Point(625, 11);
            this.secondConvolutionsContainer.Margin = new System.Windows.Forms.Padding(2);
            this.secondConvolutionsContainer.Name = "secondConvolutionsContainer";
            this.secondConvolutionsContainer.Size = new System.Drawing.Size(200, 631);
            this.secondConvolutionsContainer.TabIndex = 9;
            // 
            // secondSubsamplingContainer
            // 
            this.secondSubsamplingContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.secondSubsamplingContainer.Location = new System.Drawing.Point(829, 11);
            this.secondSubsamplingContainer.Margin = new System.Windows.Forms.Padding(2);
            this.secondSubsamplingContainer.Name = "secondSubsamplingContainer";
            this.secondSubsamplingContainer.Size = new System.Drawing.Size(140, 631);
            this.secondSubsamplingContainer.TabIndex = 10;
            // 
            // LeNetObservationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 648);
            this.Controls.Add(this.secondSubsamplingContainer);
            this.Controls.Add(this.secondConvolutionsContainer);
            this.Controls.Add(this.firstSubsamplingContainer);
            this.Controls.Add(this.firstConvolutionsContainer);
            this.Controls.Add(this.outputPicture);
            this.Controls.Add(this.consolidationPicture);
            this.Controls.Add(this.inputPicture);
            this.Name = "LeNetObservationForm";
            this.ShowIcon = false;
            this.Text = "LeNet-5 Overview";
            ((System.ComponentModel.ISupportInitialize)(this.inputPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.consolidationPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox inputPicture;
        private System.Windows.Forms.PictureBox consolidationPicture;
        private System.Windows.Forms.PictureBox outputPicture;
        private System.Windows.Forms.FlowLayoutPanel firstConvolutionsContainer;
        private System.Windows.Forms.FlowLayoutPanel firstSubsamplingContainer;
        private System.Windows.Forms.FlowLayoutPanel secondConvolutionsContainer;
        private System.Windows.Forms.FlowLayoutPanel secondSubsamplingContainer;
    }
}

