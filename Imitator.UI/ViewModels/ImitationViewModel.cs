﻿using CocomoII.Helpers;
using Imitator.Core;
using Imitator.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Imitator.UI.ViewModels
{
    class ImitationViewModel : Observable
    {
        private int minSpace = 20;
        private string avgSpaceString = $"30";
        private int maxSpace = 40;

        private int minInterline = 20;
        private string avgInterlineString = $"30";
        private int maxInterline = 40;

        private string text = "";

        private ImitatorData? data;
        private BitmapImage? image;
        private Bitmap? bitmap;

        private string folderPath = "";
        private string filename = "resultImage";

        private void Reimitate()
        {
            try
            {
                if (data != null)
                {
                    bitmap = Core.Imitator.Imitate(data, text).ToGreyscaleBitmap();
                    Image = BitmapToImageSource(bitmap);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"В процессе имитации произошла ошибка.\n{ex.Message}");
                return;
            }
        }

        private BitmapImage BitmapToImageSource(Bitmap image)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                image.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public BitmapImage? Image
        {
            get => image;
            set => Set(ref image, value);
        }

        public string Text
        {
            get => text;
            set
            {
                string editedValue = "";

                for (int i = 0; i < value.Length; i++)
                {
                    if (value[i] == '\n' || value[i] == ' ')
                    {
                        editedValue += value[i];
                    }
                    else if (Core.Imitator.WorkingSet.Contains(value[i].ToLower()))
                    {
                        editedValue += value[i];
                    }
                }

                Set(ref text, editedValue);
                Reimitate();
            }
        }

        public ImitatorData? Data
        {
            get => data;
            set
            {
                Set(ref data, value);

                if (value != null)
                {
                    minSpace = value.SpaceSize.Min;
                    OnPropertyChanged(nameof(MinSpaceString));
                    maxSpace = value.SpaceSize.Max;
                    OnPropertyChanged(nameof(MaxSpaceString));
                    avgSpaceString = value.SpaceSize.Avg.ToString();
                    OnPropertyChanged(nameof(AvgSpaceString));

                    minInterline = value.InterlineSize.Min;
                    OnPropertyChanged(nameof(MinInterlineString));
                    maxInterline = value.InterlineSize.Max;
                    OnPropertyChanged(nameof(MaxInterlineString));
                    avgInterlineString = value.InterlineSize.Avg.ToString();
                    OnPropertyChanged(nameof(AvgInterlineString));
                }
            }
        }

        public string MinSpaceString
        {
            get => minSpace.ToString();
            set
            {
                if (int.TryParse(value, out int number) && number >= 0 && number <= maxSpace && number <= AvgSpace)
                {
                    Set(ref minSpace, number);

                    if (data != null)
                    {
                        data = new ImitatorData(data.InterlineSize, (number, data.SpaceSize.Max, data.SpaceSize.Avg), data.Characters);
                        OnPropertyChanged(nameof(Data));
                        Reimitate();
                    }
                }
            }
        }

        public string MaxSpaceString
        {
            get => maxSpace.ToString();
            set
            {
                if (int.TryParse(value, out int number) && number >= 0 && minSpace <= number && AvgSpace <= number)
                {
                    Set(ref maxSpace, number);

                    if (data != null)
                    {
                        data = new ImitatorData(data.InterlineSize, (data.SpaceSize.Min, number, data.SpaceSize.Avg), data.Characters);
                        OnPropertyChanged(nameof(Data));
                        Reimitate();
                    }
                }
            }
        }

        public string AvgSpaceString
        {
            get => avgSpaceString;
            set
            {
                if (double.TryParse(value, out double number) && number >= 0 && number <= maxSpace && minSpace <= number)
                {
                    Set(ref avgSpaceString, value);

                    if (data != null)
                    {
                        data = new ImitatorData(data.InterlineSize, (data.SpaceSize.Min, data.SpaceSize.Max, number), data.Characters);
                        OnPropertyChanged(nameof(Data));
                        Reimitate();
                    }
                }
            }
        }

        public double AvgSpace
        {
            get => double.Parse(avgSpaceString);
            set => AvgSpaceString = value.ToString();
        }

        public string MinInterlineString
        {
            get => minInterline.ToString();
            set
            {
                if (int.TryParse(value, out int number) && number >= 0 && number <= maxInterline && number <= AvgInterline)
                {
                    Set(ref minInterline, number);

                    if (data != null)
                    {
                        data = new ImitatorData((number, data.InterlineSize.Max, data.InterlineSize.Avg), data.SpaceSize, data.Characters);
                        OnPropertyChanged(nameof(Data));
                        Reimitate();
                    }
                }
            }
        }

        public string MaxInterlineString
        {
            get => maxInterline.ToString();
            set
            {
                if (int.TryParse(value, out int number) && number >= 0 && minInterline <= number && AvgInterline <= number)
                {
                    Set(ref maxInterline, number);

                    if (data != null)
                    {
                        data = new ImitatorData((data.InterlineSize.Min, number, data.InterlineSize.Avg), data.SpaceSize, data.Characters);
                        OnPropertyChanged(nameof(Data));
                        Reimitate();
                    }
                }
            }
        }

        public string AvgInterlineString
        {
            get => avgInterlineString;
            set
            {
                if (double.TryParse(value, out double number) && number >= 0 && number <= maxInterline && minInterline <= number)
                {
                    Set(ref avgInterlineString, value);

                    if (data != null)
                    {
                        data = new ImitatorData((data.InterlineSize.Min, data.InterlineSize.Max, number), data.SpaceSize, data.Characters);
                        OnPropertyChanged(nameof(Data));
                        Reimitate();
                    }
                }
            }
        }

        public double AvgInterline
        {
            get => double.Parse(avgInterlineString);
            set => AvgInterlineString = value.ToString();
        }

        public string FolderPath
        {
            get => folderPath;
            set => Set(ref folderPath, value);
        }

        public string Filename
        {
            get => filename;
            set => Set(ref filename, value);
        }

        private ICommand? openFolderCommand;
        private ICommand? imitationCommand;
        private ICommand? saveCommand;

        public ICommand OpenFolderCommand => openFolderCommand ??= new RelayCommand(() =>
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    FolderPath = fbd.SelectedPath;
                }
            }
        });

        public ICommand ImitationCommand => imitationCommand ??= new RelayCommand(() =>
        {
            Reimitate();
        });

        public ICommand SaveCommand => saveCommand ??= new RelayCommand(() =>
        {
            if (bitmap == null)
            {
                MessageBox.Show("Отсутствует изображение для сохранения.");
                return;
            }

            try
            {
                string fullPath = Path.Combine(folderPath, $"{filename}.bmp");
                bitmap.Save(fullPath, ImageFormat.Bmp);
                MessageBox.Show($"Файл {fullPath} был успешно сохранён.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"В процессе сохранения изображения произошла ошибка.\n{ex.Message}");
                return;
            }
        });
    }
}