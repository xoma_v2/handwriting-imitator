﻿using CocomoII.Helpers;
using Imitator.Core;
using Imitator.Core.Extensions;
using Imitator.UI.Views;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Imitator.UI.ViewModels
{
    class SegmentationViewModel : Observable
    {
        private string samplePath = "";
        private string outputFolderPath = "";
        private string samplesText = "";
        private string dbFolderPath = "";
        private string preparationLog = "";
        private bool isWindowEnabled = true;

        public string SampplePath
        {
            get => samplePath;
            set => Set(ref samplePath, value);
        }

        public string OutputFolderPath
        {
            get => outputFolderPath;
            set => Set(ref outputFolderPath, value);
        }

        public string SamplesText
        {
            get => samplesText;
            set
            {
                string editedValue = "";

                for (int i = 0; i < value.Length; i++)
                {
                    if (value[i] == '\n' || value[i] == ' ')
                    {
                        editedValue += value[i];
                    }
                    else if (Core.Imitator.WorkingSet.Contains(value[i].ToLower()))
                    {
                        editedValue += value[i];
                    }
                }

                Set(ref samplesText, editedValue);
            }
        }

        public string DbFolderPath
        {
            get => dbFolderPath;
            set => Set(ref dbFolderPath, value);
        }

        public string PreparationLog
        {
            get => preparationLog;
            set => Set(ref preparationLog, value);
        }

        public bool IsWindowEnabled
        {
            get => isWindowEnabled;
            set => Set(ref isWindowEnabled, value);
        }

        private ICommand? openSampleCommand;
        private ICommand? openOutputFolderCommand;
        private ICommand? openDBFolderCommand;
        private ICommand? segmentationCommand;
        private ICommand? imitationCommand;

        public ICommand OpenSampleCommand => openSampleCommand ??= new RelayCommand(() =>
        {
            using (var fd = new OpenFileDialog { Filter = "JPG|*.jpg|PNG|*.png|BMP|*.bmp|GIF|*.gif|Все файлы|*.*" })
            {
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    SampplePath = fd.FileName;
                }
            }
        });

        public ICommand OpenOutputFolderCommand => openOutputFolderCommand ??= new RelayCommand(() =>
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    OutputFolderPath = fbd.SelectedPath;
                }
            }
        });

        public ICommand OpenDBFolderCommand => openDBFolderCommand ??= new RelayCommand(() =>
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    DbFolderPath = fbd.SelectedPath;
                }
            }
        });

        public ICommand SegmentationCommand => segmentationCommand ??= new RelayCommand(() =>
        {
            if (string.IsNullOrEmpty(samplePath) || string.IsNullOrWhiteSpace(samplePath))
            {
                MessageBox.Show("Отсутствует путь к изображению с образцом почерка.");
                return;
            }

            if (!File.Exists(samplePath))
            {
                MessageBox.Show("По указанному пути изображение с образцом почерка не найдено.");
                return;
            }

            if (string.IsNullOrEmpty(outputFolderPath) || string.IsNullOrWhiteSpace(outputFolderPath))
            {
                MessageBox.Show("Отсутствует путь к папке для сохранения результата.");
                return;
            }            

            Task.Run(() =>
            {
                try
                {
                    SamplesText = SamplesText.ReplaceAll("  ", " ");
                    SamplesText = SamplesText.ReplaceAll(" \n", "\n");
                    SamplesText = SamplesText.ReplaceAll("\n\n", "\n");

                    if (SamplesText.Length > 0 && samplesText[^1] == '\n')
                    {
                        SamplesText = samplesText[..^1];
                    }

                    if (string.IsNullOrEmpty(samplesText) || string.IsNullOrWhiteSpace(samplesText))
                    {
                        MessageBox.Show("Отсутствует построчная расшифровка текста.");
                        return;
                    }

                    PreparationLog = "";
                    IsWindowEnabled = false;
                    string[] lines = ((string)samplesText.Clone()).Split('\n');
                    List<string[]> text = new List<string[]>();

                    for (int i = 0; i < lines.Length; i++)
                    {
                        lines[i] = lines[i].ReplaceAll('Й', 'й');
                        lines[i] = lines[i].ReplaceAll('Ъ', 'ъ');
                        lines[i] = lines[i].ReplaceAll('Ы', 'ы');
                        lines[i] = lines[i].ReplaceAll('Ь', 'ь');

                        text.Add(lines[i].Trim().Split(' '));
                    }

                    if (Core.Imitator.TrySegmentate(samplePath, outputFolderPath, text.ToArray(), log => PreparationLog += log + "\n"))
                    {
                        DbFolderPath = Path.Combine(outputFolderPath, Path.GetFileNameWithoutExtension(samplePath), "characters");
                        MessageBox.Show($"Сегментация завершена.\nОбазятально проверьте папку {DbFolderPath} прежде чем переходить к имитациии.\nПервый символ в названии изображения - эта та буква, которую оно должно содержать.\nВ случае несовпадения - удалите изображения.");
                    }
                    else
                    {
                        MessageBox.Show("В процессе сегментации произошла ошибка. \nПосмотрите окно вывода.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"В процессе сегментации символов произошла ошибка.\n{ex.Message}");
                }

                IsWindowEnabled = true;
            });
        });

        public ICommand ImitationCommand => imitationCommand ??= new RelayCommand(() =>
        {
            if (string.IsNullOrEmpty(dbFolderPath) || string.IsNullOrWhiteSpace(dbFolderPath))
            {
                MessageBox.Show("Отсутствует путь к папке с символами и метаданными.");
                return;
            }

            if (!File.Exists(Path.Combine(dbFolderPath, "meta.dat")))
            {
                MessageBox.Show("По указанному пути файл с метаданными не найден.");
                return;
            }

            ImitatorData data;
            try
            {
                data = Core.Imitator.GetData(dbFolderPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"В процессе считывания символов и метаданных произошла ошибка.\n{ex.Message}");
                return;
            }

            try
            {
                var view = new ImitationView();
                view.DataContext = new ImitationViewModel();
                ((ImitationViewModel)view.DataContext).Data = data;
                view.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("В ходе выполнения программы роизошла ошибка.\n" + ex.Message);
            }
        });
    }
}
